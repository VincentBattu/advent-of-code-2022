import path from 'path'
import { getLines } from './file-reader.js'

const lines = await getLines('16')

interface Valve {
  label: string
  flowRate: number
}

const valves: Valve[] = []
const tunnels = new Map<string, string[]>()
const flowRates = new Map<string, number>()

for (const line of lines) {
  const parts = line.split('; ')

  const label = parts[0].slice(6, 8)
  const flowRate = parseInt(parts[0].slice(23), 10)

  valves.push({
    flowRate,
    label
  })

  let tunnelsString
  if (parts[1][22] === ' ') {
    tunnelsString = parts[1].slice(23)
  } else {
    tunnelsString = parts[1].slice(22)
  }
  tunnels.set(label, tunnelsString.split(',').map(el => el.trim()))
  flowRates.set(label, flowRate)
}

function distancesTo(startLabel: string, tunnels: Map<string, string[]>, flowRates: Map<string, number>): Record<string, number> {
  const visited = {
    [startLabel]: true
  }

  const distances: Record<string, number> = {}

  const queue = [startLabel]
  distances[startLabel] = 0

  while (queue.length !== 0) {
    const visiting = queue.shift() as string

    for (const neighbor of tunnels.get(visiting)!) {
      if (!visited[neighbor]) {
        visited[neighbor] = true
        distances[neighbor] = distances[visiting] + 1
        queue.push(neighbor)
      }
    }
  }

  return Object.entries(distances).reduce<Record<string, number>>((acc, [label, distance]) => {
    if (flowRates.get(label) !== 0 && label !== startLabel) {
      acc[label] = distance + 1
    }
    return acc
  }, {})
}


const START = 'AA'
const valvesNeedDistancesCompute = [START]

for (const valve of valves) {
  if (valve.flowRate !== 0) {
    valvesNeedDistancesCompute.push(valve.label)
  }
}

let distances: Record<string, Record<string, number>> = {}

for (const label of valvesNeedDistancesCompute) {
  distances[label] = distancesTo(label, tunnels, flowRates)
}


const allPaths: { path: string[], releasePressure: number, commingFrom: 'A' | 'B' }[] = []
function dfs(
  startLabel: string,
  remainingTime: number,
  visited: Record<string, boolean>,
  path: string[],
  releasedPressureByMinute: number,
  totalReleasedPressure: number
): void {
  visited[startLabel] = true

  if (remainingTime <= 1) {
    allPaths.push({
      path: [...path],
      releasePressure: totalReleasedPressure + releasedPressureByMinute * remainingTime,
      commingFrom: 'A'
    })
    visited[startLabel] = false
    return
  }

  let noSubPath = true
  for (const [neighbor, distance] of Object.entries(distances[startLabel])) {
    if (!visited[neighbor] && distance < remainingTime) {
      noSubPath = false
      const newRemainingTime = remainingTime - distance

      let newTotalReleasedPressure = totalReleasedPressure + releasedPressureByMinute * distance

      const newReleasedPressureByMinute = releasedPressureByMinute + flowRates.get(neighbor)!
      path.push(neighbor)
      dfs(neighbor, newRemainingTime, visited, path, newReleasedPressureByMinute, newTotalReleasedPressure)

      const index = path.indexOf(neighbor)
      path.splice(index, 1)
    }
  }

  if (noSubPath) {
    if (remainingTime >= 0) {
      allPaths.push({
        path: [...path],
        releasePressure: totalReleasedPressure + remainingTime * releasedPressureByMinute,
        commingFrom: 'B'
      })
    }

  }

  visited[startLabel] = false
}

const paths: string[] = [START]
dfs(START, 30, { AA: true }, paths, 0, 0)

allPaths.sort((a, b) => b.releasePressure - a.releasePressure)

const bestPath = allPaths[0]
console.log(bestPath.releasePressure)
