import { getLines } from './file-reader.js'

const lines = await getLines('25')



class Snafu {
  private readonly value: number

  private readonly mapping = new Map([
    ['=', -2],
    ['-', -1],
    ['0', 0],
    ['1', 1],
    ['2', 2]
  ])

  public constructor (number: number)
  public constructor (str: string)
  public constructor (arg: string | number) {
    if (typeof arg === 'string') {
      this.value = this.parseSnafu(arg)
    } else {
      this.value = arg
    }
  }

  public valueOf () {
    return this.value
  }

  public toString () {
    const digits = ['=', '-', '0', '1', '2']

    let value = this.value

    let parts: string[] = []

    while (value > 0) {
      value += 2
      const quotient = Math.floor(value / 5)
      const remainder = value % 5
      
      value = quotient

      parts.push(digits[remainder])
    }

    return parts.reverse().join('')
  }

  private parseSnafu (str: string): number {
    let power = 0
    let number = 0
    
    for (let i = str.length - 1; i >=0; i--) {
      const value = this.mapping.get(str[i])!
  
      number += value * Math.pow(5, power)
      power++
    }
  
    return number
  }
  
}

let sum = 0
for (const line of lines) {
  sum += new Snafu(line).valueOf()
}
console.log(new Snafu(sum).toString())