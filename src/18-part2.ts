import { getLines } from './file-reader.js'

const lines = await getLines('18')

interface Position {
  x: number
  y: number
  z: number
}

interface Cube {
  position: Position
  numberSidesExposed: number
}

function positionToString(position: Position) {
  return `${position.x}-${position.y}-${position.z}`
}

function getAdjacents(position: Position): Position[] {
  const { x, y, z } = position
  return [
    { x: x + 1, y, z },
    { x: x - 1, y, z },
    { x, y: y + 1, z },
    { x, y: y - 1, z },
    { x, y, z: z - 1 },
    { x, y, z: z + 1 },
  ]
}

function isCube(position: Position) {
  return cubes.some(cube => cube.position.x === position.x && cube.position.y === position.y && cube.position.z === position.z)
}

type Edges = {
  minX: number
  maxX: number
  minY: number
  maxY: number
  minZ: number
  maxZ: number
}

function getEdges(cubes: Cube[]): Edges {
  const edges =  cubes.reduce<Edges>((acc, cube) => {
    if (cube.position.x < acc.minX) {
      acc.minX = cube.position.x
    }
    if (cube.position.x > acc.maxX) {
      acc.maxX = cube.position.x
    }

    if (cube.position.y < acc.minY) {
      acc.minY = cube.position.y
    }
    if (cube.position.y > acc.maxY) {
      acc.maxY = cube.position.y
    }

    if (cube.position.z < acc.minZ) {
      acc.minZ = cube.position.z
    }
    if (cube.position.z > acc.maxZ) {
      acc.maxZ = cube.position.z
    }
    return acc
  }, { minX: Infinity, maxX: 0, minY: Infinity, maxY: 0, minZ: Infinity, maxZ: 0 })

  edges.maxX++
  edges.minX--
  edges.maxY++
  edges.minY--
  edges.maxZ++
  edges.minZ--

  return edges
}


function countExternalSides(start: Position, edges: Edges) {
  const visited = new Set<string>()

  const queue: Position[] = [start]

  let sides = 0
  while (queue.length !== 0) {
    const visiting = queue.shift() as Position

    const adjacents = getAdjacents(visiting)
      .filter(adjacent => adjacent.x >= edges.minX && adjacent.x <= edges.maxX &&
        adjacent.y >= edges.minY && adjacent.y <= edges.maxY &&
        adjacent.z >= edges.minZ && adjacent.z <= edges.maxZ)

    const hash = positionToString(visiting)
    if (!visited.has(hash)) {
      for (const adjacent of adjacents) {
        const adjacentIsACube = isCube(adjacent)

        if (adjacentIsACube) {
          sides++
        } else {
          queue.push(adjacent)
        }
      }
      visited.add(hash)
    }
  }

  return sides
}

const cubes = lines.map<Cube>(line => {
  const [x, y, z] = line.split(',').map(el => parseInt(el, 10))

  return {
    position: { x, y, z },
    numberSidesExposed: 6
  }
})

const edges = getEdges(cubes)
const startingPosition: Position = {
  x: edges.minX,
  y: edges.minY,
  z: edges.minZ
}

console.log(countExternalSides(startingPosition, edges))
