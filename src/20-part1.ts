import { getLines } from './file-reader.js'

const lines = await getLines('20')

const list = lines.map(line => ({
  visited: false,
  value: parseInt(line, 10)
}))


let currentIndex = 0

while (currentIndex !== list.length) {
  const currentElement = list[currentIndex]

  if (currentElement.visited || currentElement.value === 0) {
    currentIndex++
    currentElement.visited = true
    continue
  }

  let newIndex = (currentIndex + currentElement.value) % (list.length - 1)

  if (newIndex < 0) {
    newIndex = newIndex + list.length - 1
  }
  
  if (newIndex < currentIndex) {
    list.splice(newIndex, 0, currentElement)
    list.splice(currentIndex + 1, 1)
    currentIndex++
  } else if (newIndex > currentIndex) {
    list.splice(currentIndex, 1)
    list.splice(newIndex, 0, currentElement)
  } else {
    currentIndex++
  }
  currentElement.visited = true
}

const finalList = list.map(element => element.value)
const indexOfZero = finalList.indexOf(0)

console.log(finalList[(indexOfZero + 1_000) % finalList.length] + finalList[(indexOfZero + 2_000) % finalList.length] + finalList[(indexOfZero + 3_000) % finalList.length])