import { getLines } from './file-reader.js'

const lines = await getLines('06')

const input = lines[0]

for (let i = 4; i < input.length; i++) {
  const slice = input.slice(i - 4, i)
  if (!hasDuplicate(slice)) {
    console.log(i)
    break
  }
}


function hasDuplicate (data: string) {
  return new Set(data).size !== data.length
}