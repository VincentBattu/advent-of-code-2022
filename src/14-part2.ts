import { setTimeout } from 'node:timers/promises'
import { getLines } from './file-reader.js'

const lines = await getLines('14')

enum Tile {
  Rock = '#',
  Air = '.',
  Source = '+',
  Sand = 'o'
}

interface Coordinate {
  x: number
  y: number
}

class CaveMap {
  private sourcePosition: Coordinate = { x: 500, y: 0 }

  private minY = Infinity
  private minX = Infinity

  private maxY = this.sourcePosition.y
  private maxX = this.sourcePosition.x

  private tiles: Tile[][] = []

  public constructor(paths: string[]) {
    for (const path of paths) {
      const paths = path.split(' -> ')

      for (let i = 1; i < paths.length; i++) {
        const from = paths[i - 1]
        const to = paths[i]
        const [fromX, fromY] = from.split(',').map(el => parseInt(el, 10))
        const [toX, toY] = to.split(',').map(el => parseInt(el, 10))

        this.addRockLine({ x: fromX, y: fromY }, { x: toX, y: toY })
      }
    }

    this.addRockLine({ x: this.minX - 2, y: this.maxY + 1 }, { x: this.maxX + 200, y: this.maxY + 1 })

    this.addTile(this.sourcePosition, Tile.Source)
    this.fillWithAir()
  }

  public async produceSand(tick?: () => Promise<void>): Promise<'rest' | 'block_source'> {
    let sandCoordinate = this.sourcePosition
    this.setTile(sandCoordinate, Tile.Sand)
    if (tick) {
      await tick()
    }

    while (true) {
      const newSandCoordinate = this.moveSand(sandCoordinate)

      const blcokSource = newSandCoordinate.y === this.sourcePosition.y && newSandCoordinate.x === this.sourcePosition.x
      if (blcokSource) {
        return 'block_source'
      }

      const comesToRest =  newSandCoordinate.y === this.maxY + 1 || (newSandCoordinate.x === sandCoordinate.x && newSandCoordinate.y === sandCoordinate.y)
      if (comesToRest) {
        return 'rest'
      }
      sandCoordinate = newSandCoordinate


      if (tick) {
        await tick()
      }
    }
  }

  public toString() {
    let result = ''

    for (let y = this.minY; y <= this.maxY; y++) {
      for (let x = this.minX - 1; x <= this.maxX; x++) {
        result += this.tiles[y][x]
      }
      result += '\n'
    }

    return result
  }

  private addRockLine (from: Coordinate, to: Coordinate) {
    if (from.y === to.y) {
      const y = from.y
      let minX = Math.min(from.x, to.x)
      let maxX = Math.max(from.x, to.x)

      for (let x = minX; x <= maxX; x++) {
        this.addTile({ x, y }, Tile.Rock)
      }
    } else if (from.x === to.x) {
      const x = from.x

      let minY = Math.min(from.y, to.y)
      let maxY = Math.max(from.y, to.y)

      for (let y = minY; y <= maxY; y++) {
        this.addTile({ x, y }, Tile.Rock)
      }
    }
  }

  private moveSand(sandPosition: Coordinate): Coordinate {
    if (this.getTile({ x: sandPosition.x, y: sandPosition.y + 1 }) === Tile.Air) {
      this.setTile(sandPosition, Tile.Air)
      const newSandPosition: Coordinate = { x: sandPosition.x, y: sandPosition.y + 1 }
      this.setTile(newSandPosition, Tile.Sand)
      return newSandPosition
    }

    if (this.getTile({ x: sandPosition.x - 1, y: sandPosition.y + 1 }) === Tile.Air) {
      this.setTile(sandPosition, Tile.Air)
      const newSandPosition: Coordinate = { x: sandPosition.x - 1, y: sandPosition.y + 1 }
      this.setTile(newSandPosition, Tile.Sand)
      return newSandPosition
    }

    if (this.getTile({ x: sandPosition.x + 1, y: sandPosition.y + 1 }) === Tile.Air) {
      this.setTile(sandPosition, Tile.Air)
      const newSandPosition: Coordinate = { x: sandPosition.x + 1, y: sandPosition.y + 1 }
      this.setTile(newSandPosition, Tile.Sand)
      return newSandPosition
    }

    return sandPosition
  }

  private getTile(coordinate: Coordinate): Tile {
    return this.tiles[coordinate.y][coordinate.x]
  }

  private setTile(coordinate: Coordinate, tile: Tile) {
    this.tiles[coordinate.y][coordinate.x] = tile
  }

  private addTile({ x, y }: Coordinate, tile: Tile) {
    if (!this.tiles[y]) {
      this.tiles[y] = []
    }
    this.tiles[y][x] = tile

    if (x >= this.maxX) {
      this.maxX = x + 1
    }
    if (x <= this.minX) {
      this.minX = x - 1
    }
    if (y >= this.maxY) {
      this.maxY = y + 1
    }
    if (y < this.minY) {
      this.minY = y
    }
  }

  private fillWithAir() {
    for (let y = 0; y <= this.maxY; y++) {
      if (!this.tiles[y]) {
        this.tiles[y] = []
      }
      for (let x = 0; x <= this.maxX; x++) {
        if (!this.tiles[y][x]) {
          this.tiles[y][x] = Tile.Air
        }
      }
    }
  }
}

const DELAY_MS = 10
const DISPLAY = false
const caveMap = new CaveMap(lines)

if (DISPLAY) {
  console.clear()
  console.log(caveMap.toString())
}

let result: 'block_source' | 'rest' = 'rest'

let counter = 0
while (result !== 'block_source') {
  if (DISPLAY) {
    result = await caveMap.produceSand(async () => {
      await setTimeout(DELAY_MS)
      console.clear()
      console.log(caveMap.toString())
    })
  } else {
    result = await caveMap.produceSand()
  }

  counter++
}


console.log(counter)