import { getLines } from './file-reader.js'

const lines = await getLines('13')

type Packet = (number | Packet)[]

function compare (packet1: Packet, packet2: Packet): number {
  const leftValue = packet1[0]
  const rightValue = packet2[0]

  if (leftValue === undefined && rightValue !== undefined) {
    return -1
  }

  if (rightValue === undefined && leftValue !== undefined) {
    return 1
  }

  if (rightValue === undefined && leftValue === undefined) {
    return 0
  }

  if (typeof leftValue === 'number' && typeof rightValue === 'number') {
    if (leftValue === rightValue) {
      return compare(packet1.slice(1), packet2.slice(1))
    }
    return leftValue - rightValue
  }

  if (Array.isArray(leftValue) && Array.isArray(rightValue)) {
    const result = compare(leftValue, rightValue)

    if (result === 0) {
      return compare(packet1.slice(1), packet2.slice(1))
    }
    return result
  }

  if (typeof leftValue === 'number' && Array.isArray(rightValue)) {
    const result = compare([leftValue], rightValue)
    if (result === 0) {
      return compare(packet1.slice(1), packet2.slice(1))
    }
    return result
  }

  if (Array.isArray(leftValue) && typeof rightValue === 'number') {
    const result = compare(leftValue, [rightValue])
    if (result === 0) {
      return compare(packet1.slice(1), packet2.slice(1))
    }
    return result
  }

  throw new Error('Cannot happen')
}


const firstDivider: Packet = [[2]]
const secondDivider: Packet = [[6]]

const packets: Packet[] = [firstDivider, secondDivider]

for (const line of lines) {
  if (line.trim() !== '') {
    packets.push(JSON.parse(line))
  }
}

packets.sort(compare)

const firstDividerIndex = packets.findIndex(packet => packet === firstDivider) + 1
const secondDividerIndex = packets.findIndex(packet => packet === secondDivider) + 1

console.log(firstDividerIndex * secondDividerIndex)
