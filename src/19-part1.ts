import fs from 'node:fs/promises'

export async function getLines(fileName: string): Promise<string[]> {
  const fileContent = await fs.readFile(`input/${fileName}.txt`, 'utf-8')

  return fileContent.trim().split('\n').map(line => line.trim())
}

const enum Resource {
  Ore = 'ore',
  Clay = 'clay',
  Obisdian = 'obsidian',
  Geode = 'geode'
}

interface State {
  inventory: {
    ore: number
    clay: number
    obsidian: number
    geode: number
  }
  robots: {
    ore: number
    clay: number
    obsidian: number
    geode: number
  }
  time: number
}

interface Blueprint {
  id: number
  factories: {
    ore: { ore: number }
    clay: { ore: number }
    obsidian: { ore: number, clay: number }
    geode: { ore: number, obsidian: number }
  }
}

class GeodeCounter {
  private static readonly MAX_TIME = 24

  private cache = new Map<string, number>()

  private maxRobots

  public constructor(private readonly bluePrint: Blueprint) {
    this.maxRobots = this.computeMaxRobots(bluePrint)
  }

  public getMaxGeodes(state: State): number {
    const stateId = this.getStateId(state)
    if (this.cache.has(stateId)) {
      return this.cache.get(stateId)!
    }

    let max = state.inventory.geode

    if (state.time <= GeodeCounter.MAX_TIME) {
      for (const child of this.getNextStates(state, max)) {
        max = Math.max(max, this.getMaxGeodes(child))
      }
    }

    this.cache.set(stateId, max)
    return max
  }

  private getMaxPotential (resource: Resource, state: State) {
    const timeRemaining = GeodeCounter.MAX_TIME - state.time

    return state.inventory[resource] + timeRemaining * state.robots[resource] + (timeRemaining * (timeRemaining - 1)) / 2
  }



  private getNextStates(state: State, currentMax: number): State[] {
    const children: State[] = []

    const { inventory, robots, time } = state

    const potentialMaxGeodes = this.getMaxPotential(Resource.Geode, state)
    if (potentialMaxGeodes <= currentMax) {
      return []
    }

    // const potentialMaxObsidian = inventory.obsidian + timeRemaining * robots.obsidian + (timeRemaining * (timeRemaining - 1)) / 2
    // if (potentialMaxObsidian < maxRobots.obsidian) {
    //   return  []
    // }

    if (inventory.ore >= this.bluePrint.factories.geode.ore && inventory.obsidian >= this.bluePrint.factories.geode.obsidian) {
      children.push({
        robots: {
          ...robots,
          geode: robots.geode + 1
        },
        inventory: {
          clay: inventory.clay + robots.clay,
          geode: inventory.geode + robots.geode,
          obsidian: inventory.obsidian - this.bluePrint.factories.geode.obsidian + robots.obsidian,
          ore: inventory.ore - this.bluePrint.factories.geode.ore + robots.ore
        },
        time: time + 1
      })
      return children
    }

    if (inventory.ore >= this.bluePrint.factories.ore.ore && robots.ore < this.maxRobots.ore) {
      children.push({
        robots: {
          ...robots,
          ore: robots.ore + 1
        },
        inventory: {
          clay: inventory.clay + robots.clay,
          geode: inventory.geode + robots.geode,
          obsidian: inventory.obsidian + robots.obsidian,
          ore: inventory.ore - this.bluePrint.factories.ore.ore + robots.ore
        },
        time: time + 1
      })
    }

    if (inventory.ore >= this.bluePrint.factories.clay.ore && robots.clay < this.maxRobots.clay) {
      children.push({
        robots: {
          ...robots,
          clay: robots.clay + 1
        },
        inventory: {
          clay: inventory.clay + robots.clay,
          geode: inventory.geode + robots.geode,
          obsidian: inventory.obsidian + robots.obsidian,
          ore: inventory.ore - this.bluePrint.factories.clay.ore + robots.ore
        },
        time: time + 1
      })
    }

    if (inventory.ore >= this.bluePrint.factories.obsidian.ore && inventory.clay >= this.bluePrint.factories.obsidian.clay && robots.obsidian < this.maxRobots.obsidian) {
      children.push({
        robots: {
          ...robots,
          obsidian: robots.obsidian + 1
        },
        inventory: {
          clay: inventory.clay - this.bluePrint.factories.obsidian.clay + robots.clay,
          geode: inventory.geode + robots.geode,
          obsidian: inventory.obsidian + robots.obsidian,
          ore: inventory.ore - this.bluePrint.factories.obsidian.ore + robots.ore
        },
        time: time + 1
      })
    }

    children.push({
      robots: {
        ...robots
      },
      inventory: {
        clay: inventory.clay + robots.clay,
        geode: inventory.geode + robots.geode,
        obsidian: inventory.obsidian + robots.obsidian,
        ore: inventory.ore + robots.ore
      },
      time: time + 1
    })

    return children
  }


  private getStateId(state: State) {
    const { time, inventory, robots } = state
    return `${time}-${inventory.ore}-${inventory.clay}-${inventory.obsidian}-${inventory.geode}-${robots.ore}-${robots.clay}-${robots.obsidian}-${robots.geode}`
  }

  private computeMaxRobots(bluePrint: Blueprint) {
    return {
      ore: Math.max(bluePrint.factories.ore.ore, bluePrint.factories.geode.ore, bluePrint.factories.obsidian.ore, bluePrint.factories.geode.ore),
      clay: bluePrint.factories.obsidian.clay,
      obsidian: bluePrint.factories.geode.obsidian
    }
  }
}

function parse(line: string): Blueprint {
  const parseRegex = /Blueprint (\d+): Each ore robot costs (\d+) ore. Each clay robot costs (\d+) ore. Each obsidian robot costs (\d+) ore and (\d+) clay. Each geode robot costs (\d+) ore and (\d+) obsidian./

  const result = line.match(parseRegex)!

  return {
    id: parseInt(result[1], 10),
    factories: {
      ore: {
        ore: parseInt(result[2], 10)
      },
      clay: {
        ore: parseInt(result[3], 10)
      },
      obsidian: {
        ore: parseInt(result[4], 10),
        clay: parseInt(result[5], 10)
      },
      geode: {
        ore: parseInt(result[6], 10),
        obsidian: parseInt(result[7], 10)
      }
    }
  }
}

const lines = await getLines('19')

console.time()
let sum = 0
for (const line of lines) {
  const blueprint = parse(line)

  const startingState: State = {
    inventory: {
      clay: 0,
      geode: 0,
      obsidian: 0,
      ore: 0
    },
    robots: {
      clay: 0,
      geode: 0,
      obsidian: 0,
      ore: 1
    },
    time: 0
  }

  const maxGeode = new GeodeCounter(blueprint).getMaxGeodes(startingState)
  sum += blueprint.id * maxGeode
}

console.log(sum)

console.timeEnd()
