import { getLines } from './file-reader.js'

const lines = await getLines('17')

enum Direction {
  Right = '>',
  Left = '<'
}

class PushGaz {
  public i = 0

  public pattern: Direction[] = []

  public constructor (line: string) {
    for (const character of line) {
      this.pattern.push(character as Direction)
    }
  }

  public nextPush (): Direction {
    return this.pattern[this.i++ % this.pattern.length]
  }
}

enum RockType {
  HorizontalLine,
  Plus,
  ReverseL,
  VerticalLine,
  Block
}

class RockTypeGenerator {
  public i = 0

  public pattern: RockType[] = [RockType.HorizontalLine, RockType.Plus, RockType.ReverseL, RockType.VerticalLine, RockType.Block]

  public nextRock (): RockType {
    return this.pattern[this.i++ % this.pattern.length]
  }
}

enum Tile {
  Air = '.',
  Rock = '#',
  Floor = '-',
}

interface Position {
  x: number
  y: number
}

class Rock {
  public rockPositions: Position[] = []

  public constructor (private readonly chamber: Chamber, type: RockType) {
    if (type === RockType.HorizontalLine) {
      const y = this.chamber.heighestRockY + 4
      const x = 2

      for (let i = 0; i < 4; i++) {
        const position: Position = { y, x: x + i }
        this.chamber.add(position, Tile.Rock)
        this.rockPositions.push(position)
      }
    } else if (type === RockType.Plus) {
      const y = this.chamber.heighestRockY + 5 
      const x = 3

      this.rockPositions.push(this.chamber.add({ x, y }, Tile.Rock))
      this.rockPositions.push(this.chamber.add({ x: x + 1, y }, Tile.Rock))
      this.rockPositions.push(this.chamber.add({ x: x - 1, y }, Tile.Rock))
      this.rockPositions.push(this.chamber.add({ x, y: y + 1 }, Tile.Rock))
      this.rockPositions.push(this.chamber.add({ x, y: y - 1 }, Tile.Rock))
    } else if (type === RockType.ReverseL) {
      const y = this.chamber.heighestRockY + 4
      const x = 2

      this.rockPositions.push(this.chamber.add({ x, y }, Tile.Rock))
      this.rockPositions.push(this.chamber.add({ x: x + 1, y }, Tile.Rock))
      this.rockPositions.push(this.chamber.add({ x: x + 2, y }, Tile.Rock))
      this.rockPositions.push(this.chamber.add({ x: x + 2, y: y + 1 }, Tile.Rock))
      this.rockPositions.push(this.chamber.add({ x: x + 2, y: y + 2 }, Tile.Rock))
    } else if (type === RockType.VerticalLine) {
      const x = 2

      for (let y = this.chamber.heighestRockY + 4; y < this.chamber.heighestRockY + 4 + 4; y++) {
        this.rockPositions.push(this.chamber.add({ x, y }, Tile.Rock))
      }
    } else if (type === RockType.Block) {
      const y = this.chamber.heighestRockY + 4
      const x = 2

      this.rockPositions.push(this.chamber.add({ x, y }, Tile.Rock))
      this.rockPositions.push(this.chamber.add({ x: x + 1, y }, Tile.Rock))
      this.rockPositions.push(this.chamber.add({ x: x + 1, y: y + 1 }, Tile.Rock))
      this.rockPositions.push(this.chamber.add({ x, y: y + 1 }, Tile.Rock))
    }
  }

  public moveRight () {
    const { right } = this.getEdges()

    const canMove = right + 1 < this.chamber.width && this.rockPositions.every(rockPosition => {
      return this.chamber.tiles[rockPosition.y][rockPosition.x + 1] === Tile.Air || 
        this.rockPositions.some(otherPosition => rockPosition.y === otherPosition.y && rockPosition.x + 1 === otherPosition.x)
    })

    if (canMove) {
      for (const rockPosition of this.rockPositions) {
        this.chamber.tiles[rockPosition.y][rockPosition.x] = Tile.Air
        rockPosition.x++
      }
      this.putInChamber()
    }
  }

  public moveLeft () {
    const { left } = this.getEdges()

    const canMove = left - 1 >= 0 && this.rockPositions.every(rockPosition => {
      return this.chamber.tiles[rockPosition.y][rockPosition.x - 1] === Tile.Air || 
        this.rockPositions.some(otherPosition => rockPosition.y === otherPosition.y && rockPosition.x - 1 === otherPosition.x)
    })

    if (canMove) {
      for (const rockPosition of this.rockPositions) {
        this.chamber.tiles[rockPosition.y][rockPosition.x] = Tile.Air
        rockPosition.x--
      }
      this.putInChamber()
    }
  }

  public moveBottom (): boolean {
    const canFall = this.rockPositions.every(rockPosition => {
      return this.chamber.tiles[rockPosition.y - 1][rockPosition.x] === Tile.Air || 
        this.rockPositions.some(otherPosition => rockPosition.y - 1 === otherPosition.y && rockPosition.x === otherPosition.x)
    })

    if (!canFall) {
      return false
    }

    for (const rockPosition of this.rockPositions) {
      this.chamber.tiles[rockPosition.y][rockPosition.x] = Tile.Air
      rockPosition.y--
    }
    this.putInChamber()

    return true
  }

  private putInChamber () {
    for (const rockPosition of this.rockPositions) {
      this.chamber.tiles[rockPosition.y][rockPosition.x] = Tile.Rock
    }
  }

  public getEdges () {
    return this.rockPositions.reduce(({ top, bottom, left, right }, rockPosition) => {
      return {
        top: rockPosition.y > top ? rockPosition.y : top,
        bottom: rockPosition.y < bottom ? rockPosition.y : bottom,
        right: rockPosition.x > right ? rockPosition.x : right,
        left: rockPosition.x < left ? rockPosition.x : left
      }
    }, { top: 0, bottom: Infinity, left: Infinity, right: 0 })
  }
}

class Chamber {
  public width = 7

  public tiles: Tile[][] = []

  public heighestRockY = 0

  public constructor (private readonly pushGaz: PushGaz) {
    this.tiles[0] = new Array(this.width).fill(Tile.Floor)

    for (let i = 1; i <= 3; i++) {
      this.tiles[i] = new Array(this.width).fill(Tile.Air)
    }
  }

  public toString () {
    let result = ''
    for (let i = this.tiles.length - 1; i > 0; i--) {
      result += `|${this.tiles[i].join('')}|\n`
    }
    result += `+${this.tiles[0].join('')}+`
    return result
  }

  public getCeiling () {
    const map = new Map([
      [0, false],
      [1, false],
      [2, false],
      [3, false],
      [4, false],
      [5, false],
      [6, false],
    ])

    let minY = this.heighestRockY
    while (![ ...map.values()].every(found => found)) {
      for (let x = 0; x < 7; x++) {
        if (!map.get(x) && this.tiles[minY][x] !== Tile.Air) {
          map.set(x, true)
        }
      }
      minY--
    }

    let result = ''
    for (let y = this.heighestRockY; y > minY; y--) {
      result += `|${this.tiles[y].join('')}|\n`
    }

    return result
  }

  public add (position: Position, tile: Tile) {
    if (!this.tiles[position.y]) {
      this.tiles[position.y] = new Array(this.width).fill(Tile.Air)
    }
    this.tiles[position.y][position.x] = tile

    return position
  }

  public fallUntilRest (rock: Rock) {
    while (true) {
      const direction = this.pushGaz.nextPush()
      this.applyPushGaz(rock, direction)
      const hasMoved = rock.moveBottom()
      if (!hasMoved) {
        const { top } = rock.getEdges()
        if (top > this.heighestRockY) {
          this.heighestRockY = top
        }
        return
      }
    }
  }

  private applyPushGaz (rock: Rock, direction: Direction)  {
    if (direction === Direction.Right) {
      rock.moveRight()
    } else {
      rock.moveLeft()
    }
  }
}

const pushGaz = new PushGaz(lines[0])
const chamber = new Chamber(pushGaz)
const rockTypeGenerator = new RockTypeGenerator()

const previousState = new Map<string, { numberOfRocks: number, heighestRockY: number }>()
const heightRockY = new Map<number, number>()

let numberOfRocks = 0
let done = false
while (!done) {
  const rock = new Rock(chamber, rockTypeGenerator.nextRock())

  chamber.fallUntilRest(rock)
  numberOfRocks++

  heightRockY.set(numberOfRocks, chamber.heighestRockY)

  const key = `${chamber.getCeiling()}-${pushGaz.i % pushGaz.pattern.length}-${rockTypeGenerator.i % rockTypeGenerator.pattern.length}`
  if (previousState.get(key)) {
    const numberOfRocksInCycle = numberOfRocks - previousState.get(key)!.numberOfRocks
    const remainingNumberOfRocks = 1_000_000_000_000n - BigInt(numberOfRocksInCycle) - BigInt(previousState.get(key)!.numberOfRocks)

    const cycleHeightIncrease = chamber.heighestRockY - previousState.get(key)!.heighestRockY

    const numberOfCycleRepetitions = remainingNumberOfRocks / BigInt(numberOfRocksInCycle)
    const heightIncreaseForCycleRepetitions = numberOfCycleRepetitions * BigInt(cycleHeightIncrease)

    const remainingRocksAfterCycle = remainingNumberOfRocks % BigInt(numberOfRocksInCycle)
    const heightIncreaseInRemaining = heightRockY.get(previousState.get(key)!.numberOfRocks + Number(remainingRocksAfterCycle))! - previousState.get(key)!.heighestRockY
    const heightBeforeCyclesStart = BigInt(previousState.get(key)!.heighestRockY)

    const result = heightBeforeCyclesStart + heightIncreaseForCycleRepetitions + BigInt(cycleHeightIncrease) + BigInt(heightIncreaseInRemaining)

    done = true
    console.log(result)
  }

  previousState.set(key, { heighestRockY: chamber.heighestRockY, numberOfRocks })
}
