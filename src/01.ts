import { getLines } from './file-reader.js'

const lines = await getLines('01')


let sums = []
let sum = 0
for (const line of lines) {
  if (line.length === 0) {
    sums.push(sum)
    sum = 0
  } else {
    sum += parseInt(line, 10)
  }
}

sums.push(sum)

sums.sort().reverse()

console.log(sums[0] + sums[1] + sums[2])