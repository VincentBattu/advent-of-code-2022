import { getLines } from './file-reader.js'

const lines = await getLines('04')

class SectionsRange {
  private min: number
  private max: number

  public constructor(data: string) {
    [this.min, this.max] = data.split('-').map(d => parseInt(d, 10))
  }

  public isFullyIncludedIn(section: SectionsRange) {
    return this.min >= section.min && this.max <= section.max
  }

  public overlap(sectionsRange: SectionsRange): boolean {
    return sectionsRange.includeSection(this.min) ||
      sectionsRange.includeSection(this.max) ||
      this.includeSection(sectionsRange.min) ||
      this.includeSection(sectionsRange.max)
  }

  private includeSection(section: number) {
    return section >= this.min && section <= this.max
  }
}

let sum = 0
for (const line of lines) {
  const [firstElfSectionsData, secondElfSectionsData] = line.split(',')
  const firstElfSectionsRange = new SectionsRange(firstElfSectionsData)
  const secondElfSectionsRange = new SectionsRange(secondElfSectionsData)

  sum += +firstElfSectionsRange.overlap(secondElfSectionsRange)
}
console.log(sum)
