import { getLines } from './file-reader.js'

const lines = await getLines('03')

function getPriority(character: string): number {
  const charCode = character.charCodeAt(0)

  return charCode >= 97 ? charCode - 96 : charCode - 64 + 26
}

function getCommonItem(firstCompartementItems: string, secondCompartementItems: string): string {
  for (const firstCompartementItem of firstCompartementItems) {
    if (secondCompartementItems.includes(firstCompartementItem)) {
      return firstCompartementItem
    }
  }
  throw new Error('Invalid input data')
}

let sum = 0
for (const line of lines) {
  const numberOfItems = line.length
  const halfNumberOfItems = numberOfItems / 2

  const firstCompartementItems = line.slice(0, halfNumberOfItems)
  const secondCompartementItems = line.slice(halfNumberOfItems)

  const commonItem = getCommonItem(firstCompartementItems, secondCompartementItems)
  sum += getPriority(commonItem)
}

console.log(sum)
