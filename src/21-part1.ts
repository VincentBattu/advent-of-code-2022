import { readFile } from 'node:fs/promises'

const input = await readFile('data.txt', 'utf-8')

const monkeyJobs = new Map<string, string | number>()

for (const line of input.split('\n')) {
  const [name, operation] = line.split(': ')

  const right = parseInt(operation)

  if (isNaN(right)) {
    monkeyJobs.set(name, operation)
  } else {
    monkeyJobs.set(name, right)
  }
}

function getValue (name: string): number {
  const job = monkeyJobs.get(name)!

  if (typeof job === 'number') {
    return job
  }

  const [left, operator, right] = job.split(' ')

  if (operator === '+') {
    return getValue(left) + getValue(right)
  }

  if (operator === '-') {
    return getValue(left) - getValue(right)
  }

  if (operator === '*') {
    return getValue(left) * getValue(right)
  }

  if (operator === '/') {
    return getValue(left) / getValue(right)
  }

  throw new Error('Cannot happen')
}

console.log(getValue('root'))
