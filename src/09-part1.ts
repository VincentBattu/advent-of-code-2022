import { getLines } from './file-reader.js'

const lines = await getLines('09')

interface Position {
  x: number
  y: number
}

type Direction =  'R' | 'L' | 'U' | 'D'

class Instruction {
  public direction: Direction
  public quantity: number

  public constructor (line: string) {
    const parts = line.split(' ')
    this.direction = parts[0] as any
    this.quantity = parseInt(parts[1], 10)
  }
}

/** 
 * Contains coordinates concatenation : `xy`
 */
const visited = new Set<string>()

function addVisited (position: Position) {
  visited.add(`${position.x}${position.y}`)
}

const headPosition: Position = {
  x: 1_000,
  y: 1_000
}

let tailPosition: Position = {
  x: 1_000,
  y: 1_000
}

function moveHead (direction: Direction) {
  switch (direction) {
    case 'R':
      headPosition.x++
      break
    case 'L':
      headPosition.x--
      break
    case 'U':
      headPosition.y++
      break
    case 'D':
      headPosition.y--
      break
  }
}

function moveTail (previousHeadPosition: Position) {
  if (!isTailCloseEnough()) {
    if (headPosition.x === tailPosition.x) {
       tailPosition.y = headPosition.y > tailPosition.y ? headPosition.y - 1 : headPosition.y + 1                                                                    
    } else if (headPosition.y === tailPosition.y) {
      tailPosition.x = headPosition.x > tailPosition.x ? headPosition.x - 1 : headPosition.x + 1
    } else {
      tailPosition = previousHeadPosition
    }

    addVisited(tailPosition)
  }
}

function isTailCloseEnough () {
  let minX = headPosition.x - 1
  let maxX = headPosition.x + 1

  let minY = headPosition.y -1
  let maxY = headPosition.y + 1

  return tailPosition.x >= minX && tailPosition.x <= maxX && tailPosition.y >= minY && tailPosition.y <= maxY
}

addVisited(headPosition)
for (const line of lines) {
  const instruction = new Instruction(line)

  for (let i = 0; i < instruction.quantity; i++) {
    const previousHeadPosition = { ...headPosition }
    moveHead(instruction.direction)
    moveTail(previousHeadPosition)
  }
}

console.log(visited.size)