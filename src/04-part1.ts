import { getLines } from './file-reader.js'

const lines = await getLines('04')

class SectionsRange {
  private min: number
  private max: number

  public constructor(data: string) {
    [this.min, this.max] = data.split('-').map(d => parseInt(d, 10))
  }

  public isFullyIncludedIn(section: SectionsRange) {
    return this.min >= section.min && this.max <= section.max
  }
}

let sum = 0
for (const line of lines) {
  const [firstElfSectionsData, secondElfSectionsData] = line.split(',')
  const firstElfSectionsRange = new SectionsRange(firstElfSectionsData)
  const secondElfSectionsRange = new SectionsRange(secondElfSectionsData)

  sum += +(firstElfSectionsRange.isFullyIncludedIn(secondElfSectionsRange) || secondElfSectionsRange.isFullyIncludedIn(firstElfSectionsRange))
}

console.log(sum)
