import { getLines } from './file-reader.js'

const lines = await getLines('12')

interface Coordinate { 
  x: number
  y: number
}

class Graph {
  private neighbors: Record<string, string[]> = {}

  private startValue!: string

  public constructor (private readonly lines: string[]) {
    const mapWidth = lines[0].length
    const mapHeight = lines.length
    
    for (let y = 0; y < mapHeight; y++) {
      for (let x = 0; x < mapWidth; x++) {
        
        if (lines[y][x] === 'E') {
          this.startValue = this.verticeValue({ x, y })
        }

        const value = this.verticeValue({ x, y })
        if (!(value in this.neighbors)) {
          this.neighbors[value] = []
        }
    
        if (x - 1 >= 0) {
          this.addEdge({ x, y }, { x: x - 1, y })
        }
        if (x + 1 < mapWidth) {
          this.addEdge({ x, y }, { x: x + 1, y })
        }
        if (y - 1 >= 0) {
          this.addEdge({ x, y }, { x, y: y - 1 })
        }
        if (y + 1 < mapHeight) {
          this.addEdge({ x, y }, { x, y: y + 1 })
        }
      }
    }
  }

  public shortestPath (): number {
    const visited: Record<string, boolean> = {
      [this.startValue]: true
    }
  
    const distances: Record<string, number> = {}

    visited[this.startValue] = true
    distances[this.startValue] = 0
  
    const queue = [this.startValue]
    while (queue.length !== 0) {
      const visiting = queue.shift() as string
  
      for (const neighbor of this.neighbors[visiting]) {
        if (!visited[neighbor]) {
          visited[neighbor] = true
          distances[neighbor] = distances[visiting] + 1
          queue.push(neighbor)
  
          const [x, y] = neighbor.split('-').map(el => parseInt(el, 10))
          if (lines[y][x] === 'a' || lines[y][x] === 'S') {
            return distances[neighbor]
          }
        }
      }
    }
  
    throw new Error (`No path from ${this.startValue} to a "a" or "S" value`)
  }

  private verticeValue ({ x, y }: Coordinate) {
    return `${x}-${y}`
  }

  private addEdge (from: Coordinate, to: Coordinate) {
    const elevationGain = this.getElevationGain(from, to)
    if (elevationGain <= 1) {
      const fromValue = this.verticeValue(from)
      const toValue = this.verticeValue(to)
  
      this.neighbors[fromValue].push(toValue)
    }
  }

  private getElevationGain (from: Coordinate, to: Coordinate) {
    const fromHeight = this.getHeight(lines[from.y][from.x])
    const toHeight = this.getHeight(lines[to.y][to.x])
  
    return toHeight - fromHeight
  }

  private getHeight (letterElevation: string) {
    if (letterElevation === 'S') {
      letterElevation = 'a'
    } else if (letterElevation === 'E') {
      letterElevation = 'z'
    }
  
    return (letterElevation.charCodeAt(0) - 96) * -1 + ('z'.charCodeAt(0) - 95)
  }
}

console.log(new Graph(lines).shortestPath())