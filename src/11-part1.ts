import fs from 'node:fs/promises'

let fileContent = await fs.readFile('./input/11.txt', 'utf-8')
fileContent = fileContent.replaceAll('\r\n', '\n')
const monkeysDescriptions = fileContent.split('\n\n')

interface Item {
  worriedLevel: number
}

class Monkey {
  private items: Item[] = []

  private operation!: (oldValue: number) => number
  private successTest!: (value: number) => boolean
  
  private throwTo!: { success: number, fail: number }

  public numberOfInspections = 0

  public constructor (description: string, private readonly monkeys: Monkey[]) {
    const lines = description.split('\n')

    this.parseStartingItems(lines[1])
    this.parseOperation(lines[2])
    this.parseTest(lines[3])
    this.parseThrowTo(lines.slice(4))
  }

  public inspect () {
    let currentItem: Item | undefined
    while (currentItem = this.items.shift()) {
      currentItem.worriedLevel = this.operation(currentItem.worriedLevel)
      currentItem.worriedLevel = Math.floor(currentItem.worriedLevel / 3)

      const throwTo = this.successTest(currentItem.worriedLevel) ? this.throwTo.success : this.throwTo.fail

      this.monkeys[throwTo].items.push(currentItem)
      this.numberOfInspections++
    }
  }

  private parseStartingItems (line: string) {
    const parts = line.split(':')
    this.items = parts[1].split(',').map(el => {
      return {
        worriedLevel: parseInt(el.trim(), 10)
      }
    })
  }

  private parseOperation (line: string) {
    const [_, operation] = line.split('=')
    const symbols = operation.trim().split(' ').map(symbol => symbol.trim())

    this.operation = (oldValue: number) => {
      const leftSymbol = symbols[0]
      const rightSymbol = symbols[2]
      const operator: '+' | '*' = symbols[1] as any

      let leftValue = leftSymbol === 'old' ? oldValue : parseInt(leftSymbol, 10)
      let rigthValue = rightSymbol === 'old' ? oldValue : parseInt(rightSymbol, 10)
      
      switch(operator) {
        case '+':
          return leftValue + rigthValue
        case '*':
          return leftValue * rigthValue
      }
    }
  }

  private parseTest (line: string) {
    const parts = line.split(' ')
    const divisor = parseInt(parts.at(-1)!, 10)
    this.successTest = (value: number) => value % divisor === 0
  }

  private parseThrowTo (lines: string[]) {
    const success = parseInt(lines[0].split(' ').at(-1)!, 10)
    const fail = parseInt(lines[1].split(' ').at(-1)!, 10)

    this.throwTo = { success, fail }
  }
}

const monkeys: Monkey[] = []
for (const monkeyDescription of monkeysDescriptions) {
  monkeys.push(new Monkey(monkeyDescription, monkeys))
}

for (let i = 0; i < 20; i++) {
  for (const monkey of monkeys) {
    monkey.inspect()
  }
}


monkeys.sort((a, b) => b.numberOfInspections - a.numberOfInspections)
console.log(monkeys[0].numberOfInspections * monkeys[1].numberOfInspections)