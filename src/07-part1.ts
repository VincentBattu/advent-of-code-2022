import { getLines } from './file-reader.js'

const lines2 = await getLines('07')

interface File {
  size: number
  name: string
}

class Directory {
  public parent: Directory | null = null

  public files: File[] = []

  public subDirectoriesByName = new Map<string, Directory>()

  public size = 0

  public constructor(public readonly name: string) {
  }
  
  public addAndGetSubdirectory (directoryName: string): Directory {
    if (!this.subDirectoriesByName.has(directoryName)) {
      const directory = new Directory(directoryName)
      directory.parent = this
      this.subDirectoriesByName.set(directoryName, directory)
    }
    return this.subDirectoriesByName.get(directoryName)!
  }
}

class TreeBuilder {
  private rootDirectory!: Directory

  private currentDirectory!: Directory

  private lineNumber = 0

  public constructor(private readonly lines: string[]) {
  }

  /**
   * @returns root directory
   */
  public build(): Directory {
    const firstCommandArgument = this.getCommandArgument(this.lines[this.lineNumber]) 
    if (!this.isCdCommand(this.lines[this.lineNumber]) || firstCommandArgument !== '/') {
      throw new Error('First line must be "$ cd /"')
    }

    this.rootDirectory = new Directory(firstCommandArgument)
    this.currentDirectory = this.rootDirectory

    this.lineNumber++

    while (this.lineNumber < this.lines.length) {
      const line = this.getCurrentLine()
      if (this.isLsCommand(line)) {
        this.lineNumber++
        this.parseLsOutput()
      } else if (this.isCdCommand(line)) {
        const argument = this.getCommandArgument(line)

        if (argument === '..') {
          this.currentDirectory = this.currentDirectory.parent ?? this.currentDirectory
        } else {
          this.currentDirectory = this.currentDirectory.subDirectoriesByName.get(argument)!
        }

        this.lineNumber++
      }
    }

    return this.rootDirectory
  }

  private getCommandArgument(line: string) {
    return line.split(' ')[2]
  }

  private isCommand(line: string | undefined): boolean {
    return line !== undefined && line[0] === '$'
  }

  private isCdCommand(line: string) {
    return this.isCommand(line) && line.slice(2, 4) === 'cd'
  }

  private isLsCommand (line: string) {
    return this.isCommand(line) && line.slice(2, 4) === 'ls'
  }

  private parseLsOutput () {
    let line = this.getCurrentLine()

    while (this.lineNumber < this.lines.length && !this.isCommand(line)) {
      const parts = line.split(' ')

      if (parts[0] === 'dir') {
        this.currentDirectory.addAndGetSubdirectory(parts[1])
      } else {
        this.currentDirectory.files.push({
          name: parts[1],
          size: parseInt(parts[0])
        })
      }

      this.lineNumber++
      line = this.getCurrentLine()
    }
  }

  private getCurrentLine (): string {
    return this.lines[this.lineNumber]
  }
}

let totalSizeSmallDirectories = 0
/**
 * Post order traversal
 */
function computeSizes (directory: Directory | null) {
  if (directory === null) {
    return
  }

  for (const subDirectory of directory.subDirectoriesByName.values()) {
    computeSizes(subDirectory)
  }

  directory.size = directory.files.reduce((sum, file) => sum + file.size, 0) +
    [ ...directory.subDirectoriesByName.values()].reduce((sum, subDirectory) => sum + subDirectory.size, 0)

  if (directory.size < 100_000) {
    totalSizeSmallDirectories += directory.size
  }
}


const rootDirectory = new TreeBuilder(lines2).build()

computeSizes(rootDirectory)

console.log(totalSizeSmallDirectories)
