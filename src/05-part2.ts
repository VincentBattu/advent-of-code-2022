import fs from 'node:fs/promises'

class Stack {
  public static readonly INPUT_LENGTH = 4

  private crates: string[] = []

  public add(...crates: string[]) {
    this.crates = this.crates.concat(crates)
  }

  public reverse() {
    this.crates.reverse()
  }

  public pop(quantity: number) {
    return this.crates.splice(this.crates.length - quantity)
  }

  public getCrateOnTop() {
    return this.crates[this.crates.length - 1]
  }
}

class Instruction {
  public from: number
  public to: number
  public quantity: number

  public constructor(plainTextInstruction: string) {
    const parts = plainTextInstruction.split(' ')
    this.quantity = parseInt(parts[1], 10)
    this.from = parseInt(parts[3], 10)
    this.to = parseInt(parts[5], 10)
  }
}

let fileContent = await fs.readFile(`input/05.txt`, 'utf-8')
fileContent = fileContent.replaceAll('\r\n', '\n')
const [stacksContent, instructionsContent] = fileContent.split('\n\n')

const stackLines = stacksContent.split('\n')
const numberOfStacks = (stackLines[0].length + 1) / Stack.INPUT_LENGTH
const stacks: Stack[] = new Array(numberOfStacks)

for (let i = 0; i < numberOfStacks; i++) {
  stacks[i] = new Stack()
}

for (let i = 0; i < stackLines.length - 1; i++) {
  const stackLine = stackLines[i]
  for (let j = 0; j < stackLine.length; j += Stack.INPUT_LENGTH) {
    const crateWithBracket = stackLine.slice(j, j + Stack.INPUT_LENGTH).trim()

    if (crateWithBracket.length !== 0) {
      stacks[j / Stack.INPUT_LENGTH].add(crateWithBracket.slice(1, 2))
    }
  }
}

stacks.forEach(stack => stack.reverse())

const instructionLines = instructionsContent.split('\n')

for (const instructionLine of instructionLines) {
  const instruction = new Instruction(instructionLine)

  const crates = stacks[instruction.from - 1].pop(instruction.quantity)
  stacks[instruction.to - 1].add(...crates)
}


console.log(stacks.reduce((acc, curr) => acc += curr.getCrateOnTop(), ''))
