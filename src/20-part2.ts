import { getLines } from './file-reader.js'

const lines = await getLines('20')

const DECRYPTION_KEY = 811589153

const list = lines.map((line, i) => ({
  value: parseInt(line, 10) * DECRYPTION_KEY,
  currentIndex: i
}))

const initialOrder = [ ...list]

for (let i = 0; i < 10; i++) {
  for (const orderedElement of initialOrder) {
    const currentIndex = orderedElement.currentIndex
    const currentElement = list[currentIndex]
  
    let newIndex = (currentIndex + currentElement.value) % (list.length - 1)
  
    if (newIndex < 0) {
      newIndex = newIndex + list.length - 1
    }
    
    if (newIndex < currentIndex) {
      list.splice(newIndex, 0, currentElement)
      list.splice(currentIndex + 1, 1)
    } else if (newIndex > currentIndex) {
      list.splice(currentIndex, 1)
      list.splice(newIndex, 0, currentElement)
    }
    list.forEach((element, index) => element.currentIndex = index)
  }
}

const finalList = list.map(element => element.value)
const indexOfZero = finalList.indexOf(0)

console.log(finalList[(indexOfZero + 1_000) % finalList.length] + finalList[(indexOfZero + 2_000) % finalList.length] + finalList[(indexOfZero + 3_000) % finalList.length])