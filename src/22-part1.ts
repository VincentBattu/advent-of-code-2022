import fs from 'node:fs/promises'

let fileContent = await fs.readFile('input/22.txt', 'utf-8')
fileContent = fileContent.replaceAll('\r\n', '\n')

const enum Tile {
  Open = '.',
  Solid = '#'
}

const enum Facing {
  Right = 0,
  Down = 1,
  Left = 2,
  Up = 3,
}

interface Position {
  x: number
  y: number
}

const enum Rotation {
  Right = 'R',
  Left = 'L'
}

type MoveInstruction = Rotation | number

class StrangelyShapedBoard {
  private height: number
  private width: number

  private tiles: Tile[][] = []

  private facing = Facing.Right

  private position!: Position

  public constructor(mapDescription: string) {
    const lines = mapDescription.split('\n')

    this.width = Math.max(...lines.map(line => line.length))
    this.height = lines.length

    for (let y = 0; y < this.height; y++) {
      this.tiles[y] = new Array(this.width)
    }

    for (let y = 0; y < lines.length; y++) {
      for (let x = 0; x < lines[y].length; x++) {
        if (lines[y][x] !== ' ') {
          this.tiles[y][x] = lines[y][x] as any
        }
      }
    }

    this.setToInitialPosition()
  }

  public move(moveInstruction: MoveInstruction) {
    if (typeof moveInstruction === 'number') {
      this.progress(moveInstruction)
    } else {
      this.rotate(moveInstruction)
    }
  }

  public toString() {
    let result = ``

    for (let x = 0; x < this.width + 2; x++) {
      result += '-'
    }
    result += '\n'

    for (let y = 0; y < this.height; y++) {
      result += '|'
      for (let x = 0; x < this.width; x++) {
        if (x === this.position.x && y === this.position.y) {
          result += this.facingToString(this.facing)
        } else {
          result += this.tiles[y][x] ?? ' '
        }
      }

      result += '|\n'
    }

    for (let x = 0; x < this.width + 2; x++) {
      result += '-'
    }
    return result
  }

  public getPassword () {
    return (this.position.y + 1) * 1000 + 4 * (this.position.x + 1) + this.facing 
  }

  private progress(quantity: number) {
    for (let i = 0; i < quantity; i++) {
      const hasMoved = this.singleStepProgress()
      if (!hasMoved) {
        return
      }
    }
  }

  private singleStepProgress(): boolean {
    if (this.facing === Facing.Right) {
      let nextX = this.position.x + 1

      if (nextX >= this.width || !this.tiles[this.position.y][nextX]) {
        for (let x = 0; x < this.width; x++) {
          if (this.tiles[this.position.y][x]) {
            nextX = x
            break
          }
        }
      }
      if (this.tiles[this.position.y][nextX] === Tile.Solid) {
        return false
      }
      this.position.x = nextX
      return true
    }

    if (this.facing === Facing.Left) {
      let nextX = this.position.x - 1

      if (nextX < 0 || !this.tiles[this.position.y][nextX]) {
        for (let x = this.width - 1; x >= 0; x--) {
          if (this.tiles[this.position.y][x]) {
            nextX = x
            break
          }
        }
      }
      if (this.tiles[this.position.y][nextX] === Tile.Solid) {
        return false
      }
      this.position.x = nextX
      return true
    }

    if (this.facing === Facing.Down) {
      let nextY = this.position.y + 1

      if (nextY >= this.height || !this.tiles[nextY][this.position.x]) {
        for (let y = 0; y < this.height; y++) {
          if (this.tiles[y][this.position.x]) {
            nextY = y
            break
          }
        }
      }

      if (this.tiles[nextY][this.position.x] === Tile.Solid) {
        return false
      }
      this.position.y = nextY
      return true
    }

    if (this.facing === Facing.Up) {
      let nextY = this.position.y - 1

      if (nextY < 0 || !this.tiles[nextY][this.position.x]) {
        for (let y = this.height - 1; y >= 0; y--) {
          if (this.tiles[y][this.position.x]) {
            nextY = y
            break
          }
        }
      }

      if (this.tiles[nextY][this.position.x] === Tile.Solid) {
        return false
      }
      this.position.y = nextY
      return true
    }

    const _exhaustiveCheck: never = this.facing

    return _exhaustiveCheck
  }

  private rotate(rotation: Rotation) {
    this.facing += rotation === Rotation.Right ? 1 : -1 + 4

    this.facing %= 4
  }

  private setToInitialPosition() {
    for (let y = 0; y < this.height; y++) {
      for (let x = 0; x < this.width; x++) {
        if (this.tiles[y][x] === Tile.Open) {
          this.position = {
            x,
            y
          }
          return
        }
      }
    }
  }

  private facingToString (facing: Facing) {
    switch (facing) {
      case Facing.Down:
        return 'v'
      case Facing.Left:
        return '<'
      case Facing.Right:
        return '>'
      case Facing.Up:
        return '^'
    }
  }
}

const [mapDescription, moveInstructionsDescription] = fileContent.split('\n\n')
const board = new StrangelyShapedBoard(mapDescription)


const regex = /(\d+)(L|R)?/g

const matches = moveInstructionsDescription.matchAll(regex)

for (const match of matches) {
  board.move(parseInt(match[1], 10))
  if (match[2]) {
    board.move(match[2] as any)
  }
}

console.log(board.getPassword())