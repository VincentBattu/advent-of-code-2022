import { getLines } from './file-reader.js'

const lines = await getLines('02')

enum Choice {
  Rock = 'A',
  Paper = 'B',
  Scissors = 'C'
}

enum Outcome {
  Lose = 'X',
  Draw = 'Y',
  Win = 'Z'
}

const POINTS_PER_SHAPE = new Map([
  [Choice.Rock, 1],
  [Choice.Paper, 2],
  [Choice.Scissors, 3]
])

const POINTS_PER_OUTCOME = new Map<Outcome, number>([
  [Outcome.Win, 6],
  [Outcome.Draw, 3],
  [Outcome.Lose, 0]
])

function computeMyChoice(opponentChoice: Choice, outcome: Outcome): Choice {
  if (opponentChoice === Choice.Paper) {
    switch (outcome) {
      case Outcome.Draw:
        return Choice.Paper
      case Outcome.Lose:
        return Choice.Rock
      case Outcome.Win:
        return Choice.Scissors
    }
  }

  if (opponentChoice === Choice.Rock) {
    switch (outcome) {
      case Outcome.Draw:
        return Choice.Rock
      case Outcome.Lose:
        return Choice.Scissors
      case Outcome.Win:
        return Choice.Paper
    }
  }

  if (opponentChoice === Choice.Scissors) {
    switch (outcome) {
      case Outcome.Draw:
        return Choice.Scissors
      case Outcome.Lose:
        return Choice.Paper
      case Outcome.Win:
        return Choice.Rock
    }
  }

  throw new Error()
}


let score = 0
for (const line of lines) {
  const [opponentChoice, outcome] = line.split(' ') as [Choice, Outcome]

  const choice = computeMyChoice(opponentChoice, outcome)

  score += POINTS_PER_SHAPE.get(choice)! + POINTS_PER_OUTCOME.get(outcome)!
}

console.log(score)