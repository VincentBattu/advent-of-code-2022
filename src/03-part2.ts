import { getLines } from './file-reader.js'

const lines = await getLines('03')

function getPriority(character: string): number {
  const charCode = character.charCodeAt(0)

  return charCode >= 97 ? charCode - 96 : charCode - 64 + 26
}


function getCommonItem(rucksacks: string[]): string {
  const firstRucksack = rucksacks[0]
  const remainingRucksacks = rucksacks.slice(1)

  for (const firstRucksackItem of firstRucksack) {
    if (rucksacksHaveItem(remainingRucksacks, firstRucksackItem)) {
      return firstRucksackItem
    }
  }

  throw new Error('Invalid input data')
}

function rucksackHasItem (rucksack: string, item: string): boolean {
  return rucksack.includes(item)
}

function rucksacksHaveItem (rucksacks: string[], item: string) {
  return rucksacks.every(rucksack => rucksackHasItem(rucksack, item))
}

let sum = 0
let rucksacksGroup = []
for (const line of lines) {
  rucksacksGroup.push(line)
  if (rucksacksGroup.length === 3) {
    const commonItem = getCommonItem(rucksacksGroup)
    sum += getPriority(commonItem)

    rucksacksGroup = []
  }
}

console.log(sum)
