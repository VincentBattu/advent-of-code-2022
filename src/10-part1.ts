import { getLines } from './file-reader.js'

const lines = await getLines('10')

class Cpu {
  private register = 1

  private clock = 0

  public signalStrength = 0

  public execute (line: string) {
    const parts = line.split(' ')

    const instruction: 'noop' | 'addx' = parts[0] as any
    
    if (instruction === 'noop') {
      this.nextClock()
      return
    }

    if (instruction === 'addx') {
      const argument = parseInt(parts[1], 10)

      this.nextClock()
      this.nextClock()
      this.register += argument
    }
  }

  public nextClock () {
    this.clock++
    if (this.clock % 40 === 20) {
      this.signalStrength += this.clock * this.register
    }
  }
}


const cpu = new Cpu()
for (let i = 0; i <lines.length; i++) {
  const line = lines[i]
  cpu.execute(line)
}
console.log(cpu.signalStrength)