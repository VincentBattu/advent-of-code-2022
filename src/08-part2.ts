import { getLines } from './file-reader.js'

const lines = await getLines('08')

interface Tree {
  height: number
}

interface Coordinates {
  x: number
  y: number
}

class TreeMap {
  private trees: Tree[][] = []

  private width = 0
  private height = 0

  public maxScenicScore = 0

  public constructor(lines: string[]) {
    this.width = lines.length
    this.height = lines[0].length

    for (let x = 0; x < this.width; x++) {
      const treeLine: Tree[] = []

      const parts = lines[x].split('')
      for (let y = 0; y < this.height; y++) {
        treeLine.push({
          height: parseInt(parts[y], 10)
        })
      }
      this.trees.push(treeLine)
    }

    this.computeMaxScenicScore()
  }

  private computeMaxScenicScore() {
    for (let y = 1; y < this.height - 1; y++) {
      for (let x = 1; x < this.width - 1; x++) {
        const coordinates: Coordinates = { x, y }

        const scenicScore = this.getScenicScore(coordinates)
        if (scenicScore > this.maxScenicScore) {
          this.maxScenicScore = scenicScore
        }
      }
    }
  }

  private getScenicScore(coordinates: Coordinates) {
    return this.numberOfTreesRight(coordinates) *
      this.numberOfTreesLeft(coordinates) *
      this.numberOfTreesDown(coordinates) *
      this.numberOfTreesUp(coordinates)
  }

  private numberOfTreesUp(coordinates: Coordinates) {
    const tree = this.getTree(coordinates)

    let numberOfTrees = 0
    let y = coordinates.y - 1
    let x = coordinates.x
    let shouldContinue = true
    while (shouldContinue && y >= 0) {
      numberOfTrees++
      shouldContinue = this.getTree({ x, y }).height < tree.height
      y--
    }

    return numberOfTrees
  }

  private numberOfTreesDown(coordinates: Coordinates) {
    const tree = this.getTree(coordinates)

    let numberOfTrees = 0
    let y = coordinates.y + 1
    let x = coordinates.x
    let shouldContinue = true
    while (shouldContinue && y < this.height) {
      numberOfTrees++
      shouldContinue = this.getTree({ x, y }).height < tree.height
      y++
    }

    return numberOfTrees
  }

  private numberOfTreesLeft(coordinates: Coordinates) {
    const tree = this.getTree(coordinates)

    let numberOfTrees = 0
    let y = coordinates.y
    let x = coordinates.x - 1
    let shouldContinue = true
    while (shouldContinue && x >= 0) {
      numberOfTrees++
      shouldContinue = this.getTree({ x, y }).height < tree.height
      x--
    }

    return numberOfTrees
  }

  private numberOfTreesRight(coordinates: Coordinates) {
    const tree = this.getTree(coordinates)

    let numberOfTrees = 0
    let y = coordinates.y
    let x = coordinates.x + 1
    let shouldContinue = true
    while (shouldContinue && x < this.width) {
      numberOfTrees++
      shouldContinue = this.getTree({ x, y }).height < tree.height
      x++
    }

    return numberOfTrees
  }

  private getTree({ x, y }: Coordinates) {
    return this.trees[y][x]
  }
}


const treeMap = new TreeMap(lines)

console.log(treeMap.maxScenicScore)