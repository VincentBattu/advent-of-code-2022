import { getLines } from './file-reader.js'

const lines = await getLines('09')

interface Position {
  x: number
  y: number
}

type Direction =  'R' | 'L' | 'U' | 'D'

class Instruction {
  public direction: Direction
  public quantity: number

  public constructor (line: string) {
    const parts = line.split(' ')
    this.direction = parts[0] as any
    this.quantity = parseInt(parts[1], 10)
  }
}

/** 
 * Contains coordinates concatenation : `xy`
 */
const visited = new Set<string>()

function addVisited (position: Position) {
  visited.add(`${position.x}${position.y}`)
}

const knotsPositions: Position[] = []
for (let i = 0; i < 10; i++) {
  knotsPositions.push({
    x: 1000,
    y: 1000
  })
}

function moveHead (direction: Direction) {
  const headPosition = knotsPositions[0]
  switch (direction) {
    case 'R':
      headPosition.x++
      break
    case 'L':
      headPosition.x--
      break
    case 'U':
      headPosition.y++
      break
    case 'D':
      headPosition.y--
      break
  }
}

function moveKnot (knotIndex: number) {
  if (!isKnotCloseEnough(knotIndex)) {
    const currentKnot = knotsPositions[knotIndex]
    const previousKnot = knotsPositions[knotIndex - 1]
    if (currentKnot.x === previousKnot.x) {
      currentKnot.y = previousKnot.y > currentKnot.y ? previousKnot.y - 1 : previousKnot.y + 1                                                                    
    } else if (currentKnot.y === previousKnot.y) {
      currentKnot.x = previousKnot.x > currentKnot.x ? previousKnot.x - 1 : previousKnot.x + 1
    } else {
      if (previousKnot.x > currentKnot.x) {
        currentKnot.x++
      } else {
        currentKnot.x--
      }

      if (previousKnot.y > currentKnot.y) {
        currentKnot.y++
      } else {
        currentKnot.y--
      }
    }
  }
}

function isKnotCloseEnough (knotIndex: number) {
  const currentKnow = knotsPositions[knotIndex]
  const previousKnot = knotsPositions[knotIndex - 1]

  let minX = previousKnot.x - 1
  let maxX = previousKnot.x + 1

  let minY = previousKnot.y -1
  let maxY = previousKnot.y + 1

  return currentKnow.x >= minX && currentKnow.x <= maxX && currentKnow.y >= minY && currentKnow.y <= maxY
}

addVisited(knotsPositions.at(-1)!)
for (const line of lines) {
  const instruction = new Instruction(line)

  for (let i = 0; i < instruction.quantity; i++) {
    moveHead(instruction.direction)

    for (let j = 1; j < knotsPositions.length; j++) {
      moveKnot(j)
    }
    addVisited(knotsPositions.at(-1)!)
  }
}

console.log(visited.size)