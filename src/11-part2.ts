import fs from 'node:fs/promises'

let fileContent = await fs.readFile('./input/11.txt', 'utf-8')
fileContent = fileContent.replaceAll('\r\n', '\n')
const monkeysDescriptions = fileContent.split('\n\n')

interface Item {
  worriedLevelRemainders: Map<number, number>
}

class Monkey {
  private items: Item[] = []

  private operation!: (item: Item) => Item
  private successTest!: (item: Item) => boolean

  private throwTo!: { success: number, fail: number }

  public numberOfInspections = 0

  public constructor(description: string, divisors: number[], private readonly monkeys: Monkey[]) {
    const lines = description.split('\n')

    this.parseStartingItems(lines[1], divisors)
    this.parseOperation(lines[2])
    this.parseTest(lines[3])
    this.parseThrowTo(lines.slice(4))
  }

  public inspect() {
    let currentItem: Item | undefined
    while (currentItem = this.items.shift()) {
      currentItem = this.operation(currentItem)

      const throwTo = this.successTest(currentItem) ? this.throwTo.success : this.throwTo.fail

      this.monkeys[throwTo].items.push(currentItem)
      this.numberOfInspections++
    }
  }

  public static getDivisor (testLine: string) {
    const parts = testLine.split(' ')
    return parseInt(parts.at(-1)!, 10)
  }

  private parseStartingItems(line: string, divisors: number[]) {
    const parts = line.split(':')
    this.items = parts[1].split(',').map<Item>(el => {
      const worriedLevel = parseInt(el.trim(), 10)

      const worriedLevelRemainders = new Map<number, number>()

      for (const divisor of divisors) {
        worriedLevelRemainders.set(divisor, worriedLevel % divisor)
      }

      return {
        worriedLevelRemainders
      }
    })
  }

  private parseOperation(line: string) {
    const [, operation] = line.split('=')
    const symbols = operation.trim().split(' ').map(symbol => symbol.trim())

    this.operation = (item: Item) => {
      const leftSymbol = symbols[0]
      const rightSymbol = symbols[2]
      const operator: '+' | '*' = symbols[1] as any

      for (const [divisor, value] of item.worriedLevelRemainders.entries()) {
        let leftValue = leftSymbol === 'old' ? value : parseInt(leftSymbol, 10)
        let rigthValue = rightSymbol === 'old' ? value : parseInt(rightSymbol, 10)

        switch (operator) {
          case '+':
            item.worriedLevelRemainders.set(divisor, (leftValue + rigthValue) % divisor)
            break
          case '*':
            item.worriedLevelRemainders.set(divisor, (leftValue * rigthValue) % divisor)
            break
        }
      }
      return item
    }
  }

  private parseTest(testLine: string) {
    const divisor = Monkey.getDivisor(testLine)
    this.successTest = (item: Item) => {
      return item.worriedLevelRemainders.get(divisor) === 0
    }
  }

  private parseThrowTo(lines: string[]) {
    const success = parseInt(lines[0].split(' ').at(-1)!, 10)
    const fail = parseInt(lines[1].split(' ').at(-1)!, 10)

    this.throwTo = { success, fail }
  }
}

const divisors = monkeysDescriptions.map(description => {
  const testLine = description.split('\n')[3]
  return Monkey.getDivisor(testLine)
})
const monkeys: Monkey[] = []
for (const monkeyDescription of monkeysDescriptions) {
  monkeys.push(new Monkey(monkeyDescription, divisors, monkeys))
}

for (let i = 0; i < 10_000; i++) {
  for (const monkey of monkeys) {
    monkey.inspect()
  }
}

monkeys.sort((a, b) => b.numberOfInspections - a.numberOfInspections)
console.log(monkeys[0].numberOfInspections * monkeys[1].numberOfInspections)
