import { getLines } from './file-reader.js'

const lines = await getLines('15')

interface Position {
  x: number
  y: number
}

interface Instruction {
  sensorPosition: Position
  beaconPosition: Position
}

class CaveMap {
  private instructions: Instruction[]

  public constructor(lines: string[]) {
    this.instructions = lines.map(this.parseInstruction)
  }

  public getNumberOfAirAtLine(y: number) {
    const usedPositionCounter = new Map<number, boolean>()

    for (const instruction of this.instructions) {
      if (instruction.beaconPosition.y === y) {
        usedPositionCounter.set(instruction.beaconPosition.x, true)
      }
      if (instruction.sensorPosition.y === y) {
        usedPositionCounter.set(instruction.sensorPosition.x, true)
      }
    }

    const set = new Set()
    for (const instruction of this.instructions) {
      const { sensorPosition, beaconPosition } = instruction
      const distance = this.manhattanDistance(sensorPosition, beaconPosition) + 1

      if (y > sensorPosition.y - distance && y < sensorPosition.y + distance) {
        let x = sensorPosition.x - 1

        while (this.manhattanDistance({ x, y }, sensorPosition) < distance) {
          if (!usedPositionCounter.get(x)) {
            set.add(`${x}`)
          }
          x--
        }

        x = sensorPosition.x
        while (this.manhattanDistance({ x, y }, sensorPosition) < distance) {
          if (!usedPositionCounter.get(x)) {
            set.add(`${x}`)
          }
          x++
        }
      }
    }

    return set.size
  }

  private parseInstruction(line: string): Instruction {
    const [sensorPart, beaconPart] = line.split(': ')

    const [sensorX, sensorY] = sensorPart.slice(10).split(', ').map(el => parseInt(el.slice(2), 10))
    const [beaconX, beaconY] = beaconPart.slice(21).split(', ').map(el => parseInt(el.slice(2), 10))

    return {
      sensorPosition: { x: sensorX, y: sensorY },
      beaconPosition: { x: beaconX, y: beaconY }
    }
  }

  private manhattanDistance(from: Position, to: Position): number {
    return Math.abs(from.x - to.x) + Math.abs(from.y - to.y)
  }
}

console.time()
const caveMap = new CaveMap(lines)

const count = caveMap.getNumberOfAirAtLine(2000000)
console.timeEnd()
console.log(count)

4_000_000