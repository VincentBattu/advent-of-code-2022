import { getLines } from './file-reader.js'

const lines = await getLines('18')


interface Position {
  x: number
  y: number
  z: number
}

interface Cube {
  position: Position
  numberSidesExposed: number
}

const cubes = lines.map<Cube>(line => {
  const [x, y, z] = line.split(',').map(el => parseInt(el, 10))

  return {
    position: { x, y, z},
    numberSidesExposed: 6
  }
})

function areAdjacent (cube1: Cube, cube2: Cube) {
  return (cube1.position.x === cube2.position.x && cube1.position.y === cube2.position.y && Math.abs(cube1.position.z - cube2.position.z) === 1) ||
    (cube1.position.x === cube2.position.x && cube1.position.z === cube2.position.z && Math.abs(cube1.position.y - cube2.position.y) === 1) ||
    (cube1.position.y === cube2.position.y && cube1.position.z === cube2.position.z && Math.abs(cube1.position.x - cube2.position.x) === 1)
}

for (let i = 0; i < cubes.length; i++) {
  for (let j = 0; j < cubes.length; j++) {
    if (i !== j) {
      const firstCube = cubes[i]
      const secondCube = cubes[j]


      if (areAdjacent(firstCube, secondCube)) {
        firstCube.numberSidesExposed--
      }
    }
  }
}

const totalSurface = cubes.reduce((acc, cube) => acc + cube.numberSidesExposed, 0)
console.log(totalSurface)