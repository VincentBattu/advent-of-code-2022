import { getLines } from './file-reader.js'

const lines = await getLines('15')

interface Position {
  x: number
  y: number
}

interface Instruction {
  sensorPosition: Position
  beaconPosition: Position
}

interface Sensor {
  rotatePosition: Position
  radius: number
}

class CaveMap {
  private instructions: Instruction[]

  public constructor(lines: string[]) {
    this.instructions = lines.map(this.parseInstruction)
  }

  public getBeaconPosition(): Position | null {
    const sensors = this.instructions.map<Sensor>(instruction => {
      const sensorPosition = instruction.sensorPosition
      const radius = this.manhattanDistance(sensorPosition, instruction.beaconPosition) + 1

      return {
        rotatePosition: this.rotate45deg(sensorPosition),
        radius,
      }
    })

    const squareRightX: number[] = []
    const squareBottomY: number[] = []

    for (const sensor of sensors) {
      squareRightX.push(sensor.rotatePosition.x + sensor.radius)
      squareBottomY.push(sensor.rotatePosition.y + sensor.radius)
    }

    const xCandidates: number[] = []
    const yCandidates: number[] = []

    for (const sensor of sensors) {
      const squareLeftX = sensor.rotatePosition.x - sensor.radius

      if (squareRightX.includes(squareLeftX)) {
        xCandidates.push(squareLeftX)
      }

      const squareTopY = sensor.rotatePosition.y - sensor.radius
      if (squareBottomY.includes(squareTopY)) {
        yCandidates.push(squareTopY)
      }
    }

    for (const xCandidate of xCandidates) {
      for (const yCandidate of yCandidates) {
        const notCover = sensors.every((sensor) => xCandidate <= sensor.rotatePosition.x - sensor.radius ||
          xCandidate >= sensor.rotatePosition.x + sensor.radius ||
          yCandidate <= sensor.rotatePosition.y - sensor.radius ||
          yCandidate >= sensor.rotatePosition.y + sensor.radius)

        if (notCover) {
          return this.reserverRotate({
            x: xCandidate,
            y: yCandidate
          })
        }
      }
    }


    return null
  }


  private rotate45deg(position: Position) {
    return {
      x: position.x + position.y,
      y: position.x - position.y
    }
  }

  private reserverRotate(position: Position) {
    return {
      x: (position.x + position.y) / 2,
      y: (position.x - position.y) / 2
    }
  }

  private parseInstruction(line: string): Instruction {
    const [sensorPart, beaconPart] = line.split(': ')

    const [sensorX, sensorY] = sensorPart.slice(10).split(', ').map(el => parseInt(el.slice(2), 10))
    const [beaconX, beaconY] = beaconPart.slice(21).split(', ').map(el => parseInt(el.slice(2), 10))

    return {
      sensorPosition: { x: sensorX, y: sensorY },
      beaconPosition: { x: beaconX, y: beaconY }
    }
  }

  private manhattanDistance(from: Position, to: Position): number {
    return Math.abs(from.x - to.x) + Math.abs(from.y - to.y)
  }
}

console.time('Execution time')
const caveMap = new CaveMap(lines)

const point = caveMap.getBeaconPosition()
if (point) {
  console.log(point.x * 4_000_000 + point.y)
}
console.timeEnd('Execution time')
