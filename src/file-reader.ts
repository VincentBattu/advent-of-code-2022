import fs from 'node:fs/promises'

export async function getLines(fileName: string): Promise<string[]> {
  const fileContent = await fs.readFile(`input/${fileName}.txt`, 'utf-8')

  return fileContent.trim().split('\n').map(line => line.trim())
}

