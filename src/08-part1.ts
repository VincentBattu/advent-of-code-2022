import { getLines } from './file-reader.js'

const lines = await getLines('08')

interface Tree {
  height: number
  isVisible: boolean
}

interface Coordinates {
  x: number
  y: number
}

class TreeMap {
  private trees: Tree[][] = []

  private width = 0
  private height = 0

  public numberOfVisibleTrees = 0

  public constructor (lines: string[]) {
    this.width = lines.length
    this.height = lines[0].length

    for (let x = 0; x < this.width; x++) {
      const treeLine: Tree[] = []

      const parts = lines[x].split('')
      for (let y = 0; y < this.height; y++) {
        const isVisible = x === 0 || x === lines.length - 1 || y === 0 || y === parts.length - 1
        treeLine.push({
          height: parseInt(parts[y], 10),
          isVisible
        })
        this.numberOfVisibleTrees += +isVisible
      }
      this.trees.push(treeLine)
    }

    this.computeInteriorVisibilities()
  }

  private computeInteriorVisibilities () {
    for (let y = 1; y < this.height - 1; y++) {
      for (let x = 1; x < this.width - 1; x++) {
        const coordinates: Coordinates = { x, y }
        const tree = this.getTree(coordinates)
        
        tree.isVisible = this.isVisible(coordinates)
        this.numberOfVisibleTrees += +tree.isVisible
      }
    }
  }

  private isVisible (coordinates: Coordinates) {
    return this.isVisibleFromRight(coordinates) ||
          this.isVisibleFromLeft(coordinates) ||
          this.isVisibleFromBottom(coordinates) ||
          this.isVisibleFromTop(coordinates)
  }

  private isVisibleFromTop (coordinates: Coordinates) {
    const tree = this.getTree(coordinates)
    
    let isVisible = true
    let y = coordinates.y - 1
    let x = coordinates.x
    while (isVisible && y >= 0) {
      isVisible = this.getTree({ x, y }).height < tree.height
      y--
    }

    return isVisible
  }

  private isVisibleFromBottom (coordinates: Coordinates) {
    const tree = this.getTree(coordinates)
    
    let isVisible = true
    let y = coordinates.y + 1
    let x = coordinates.x
    while (isVisible && y < this.height) {
      isVisible = this.getTree({ x, y }).height < tree.height
      y++
    }

    return isVisible
  }

   private isVisibleFromLeft (coordinates: Coordinates) {
    const tree = this.getTree(coordinates)
    
    let isVisible = true
    let y = coordinates.y
    let x = coordinates.x - 1
    while (isVisible && x >= 0) {
      isVisible = this.getTree({ x, y }).height < tree.height
      x--
    }

    return isVisible
  }

  private isVisibleFromRight (coordinates: Coordinates) {
    const tree = this.getTree(coordinates)
    
    let isVisible = true
    let y = coordinates.y
    let x = coordinates.x + 1
    while (isVisible && x < this.width) {
      isVisible = this.getTree({ x, y }).height < tree.height
      x++
    }

    return isVisible
  }
  
  private getTree ({ x, y }: Coordinates) {
    return this.trees[y][x]
  }
}


const treeMap = new TreeMap(lines)

console.log(treeMap.numberOfVisibleTrees)