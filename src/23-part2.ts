import { getLines } from './file-reader.js'
import { setTimeout } from 'node:timers/promises'

interface Position {
  x: number
  y: number
}

class ElvesMap {
  private minX = 0
  private maxX

  private minY = 0
  private maxY

  private elvesPositions = new Set<string>()

  private propositionStartIndex = 0

  private proposition = [this.proposeNorth.bind(this), this.proposeSouth.bind(this), this.proposeWest.bind(this), this.proposeEast.bind(this)]

  public constructor(lines: string[]) {
    this.maxX = lines[0].length
    this.maxY = lines.length

    for (let y = 0; y < lines.length; y++) {
      const line = lines[y]

      for (let x = 0; x < line.length; x++) {
        if (line[x] === '#') {
          this.elvesPositions.add(this.getKey({ x, y }))
        }
      }
    }
  }

  public toString() {
    let result = ''

    for (let y = this.minY; y <= this.maxY; y++) {
      for (let x = this.minX; x <= this.maxX; x++) {
        result += this.elvesPositions.has(this.getKey({ x, y })) ? '#' : '.'
      }
      result += '\n'
    }
    return result
  }

  public countGroundTile(numberOfRounds = 10) {
    for (let i = 0; i < numberOfRounds; i++) {
      this.runRound()
    }

    let sum = 0
    for (let y = this.minY; y <= this.maxY; y++) {
      for (let x = this.minX; x <= this.maxX; x++) {
        if (!this.elvesPositions.has(this.getKey({ x, y }))) {
          sum++
        }
      }
    }
    return sum
  }

  public async getFirstRoundWithNoMoves () {
    let i = 0

    let noElfMoved = false
    while (!noElfMoved) {
      noElfMoved = this.runRound().noElfMoved
      i++
    }

    return i
  }

  private runRound(): { noElfMoved: boolean } {
    const proposedPositions = new Map<string, Position[]>()
    for (const elfPositionKey of this.elvesPositions) {
      const elfPosition = this.toPosition(elfPositionKey)
      if (this.hasElvesAround(elfPosition)) {
        const proposalPosition = this.getPositionProposal(elfPosition)
        if (!proposalPosition) {
          continue
        }
        const proposalPositionKey = this.getKey(proposalPosition)

        if (!proposedPositions.has(proposalPositionKey)) {
          proposedPositions.set(proposalPositionKey, [])
        }
        proposedPositions.get(proposalPositionKey)!.push(elfPosition)
      }
    }

    let noElfMoved = true
    for (const [proposalPositionKey, elvesProposingThisPosition] of proposedPositions.entries()) {
      if (elvesProposingThisPosition.length > 1) {
        continue
      }
      noElfMoved = false

      const elfProposingThisPosition = elvesProposingThisPosition[0]
      const elfProposingThisPositionKey = this.getKey(elfProposingThisPosition)
      this.elvesPositions.delete(elfProposingThisPositionKey)

      const newElfPosition = this.toPosition(proposalPositionKey)
      this.elvesPositions.add(proposalPositionKey)

      this.minX = Math.min(newElfPosition.x, this.minX)
      this.maxX = Math.max(newElfPosition.x, this.maxX)
      this.minY = Math.min(newElfPosition.y, this.minY)
      this.maxY = Math.max(newElfPosition.y, this.maxY)
    }

    this.propositionStartIndex++

    return {
      noElfMoved
    }
  }

  private getPositionProposal(elvePosition: Position): Position | null {
    for (let i = 0; i <this.proposition.length; i++) {
      const proposition = this.proposition[(this.propositionStartIndex + i) % this.proposition.length](elvePosition)
      if (proposition) {
        return proposition
      }
    }

    return null
  }

  private proposeNorth (elvePosition: Position) {
    const northPositions: Position[] = [
      { x: elvePosition.x - 1, y: elvePosition.y - 1 },
      { x: elvePosition.x, y: elvePosition.y - 1 },
      { x: elvePosition.x + 1, y: elvePosition.y - 1 }
    ]

    const freeNorth = northPositions.every(position => !this.hasElfAtPosition(position))
    if (freeNorth) {
      return {
        x: elvePosition.x,
        y: elvePosition.y - 1
      }
    } 
  }

  private proposeSouth (elvePosition: Position) {
    const southPositions: Position[] = [
      { x: elvePosition.x - 1, y: elvePosition.y + 1 },
      { x: elvePosition.x, y: elvePosition.y + 1 },
      { x: elvePosition.x + 1, y: elvePosition.y + 1 }
    ]

    const freeSouth = southPositions.every(position => !this.hasElfAtPosition(position))
    if (freeSouth) {
      return {
        x: elvePosition.x,
        y: elvePosition.y + 1
      }
    }

  }

  private proposeWest (elvePosition: Position) {
    const westPositions: Position[] = [
      { x: elvePosition.x - 1, y: elvePosition.y - 1 },
      { x: elvePosition.x - 1, y: elvePosition.y },
      { x: elvePosition.x - 1, y: elvePosition.y + 1 }
    ]

    const freeWest = westPositions.every(position => !this.hasElfAtPosition(position))
    if (freeWest) {
      return {
        x: elvePosition.x - 1,
        y: elvePosition.y
      }
    }
  }

  private proposeEast (elvePosition: Position) {
    const eastPositions: Position[] = [
      { x: elvePosition.x + 1, y: elvePosition.y - 1 },
      { x: elvePosition.x + 1, y: elvePosition.y },
      { x: elvePosition.x + 1, y: elvePosition.y + 1 }
    ]

    const freeEast = eastPositions.every(position => !this.hasElfAtPosition(position))
    if (freeEast) {
      return {
        x: elvePosition.x + 1,
        y: elvePosition.y
      }
    }
  }


  private hasElvesAround(position: Position) {
    const positionsToTest: Position[] = [
      { x: position.x - 1, y: position.y },
      { x: position.x - 1, y: position.y - 1 },
      { x: position.x, y: position.y - 1 },
      { x: position.x + 1, y: position.y - 1 },
      { x: position.x + 1, y: position.y },
      { x: position.x + 1, y: position.y + 1 },
      { x: position.x, y: position.y + 1 },
      { x: position.x - 1, y: position.y + 1 },
    ]

    return positionsToTest.some(position => this.hasElfAtPosition(position))
  }

  private hasElfAtPosition(position: Position): boolean {
    return this.elvesPositions.has(this.getKey(position))
  }

  private getKey(position: Position) {
    return `${position.x};${position.y}`
  }

  private toPosition(key: string): Position {
    const [x, y] = key.split(';').map(el => parseInt(el, 10))

    return {
      x,
      y
    }
  }
}

const lines = await getLines('23')
const elvesMap = new ElvesMap(lines)

console.log(elvesMap.getFirstRoundWhereNoElfMoves())