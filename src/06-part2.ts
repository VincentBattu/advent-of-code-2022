import { getLines } from './file-reader.js'

const lines = await getLines('06')

const input = lines[0]

const MARKER_LENGTH = 14
for (let i = MARKER_LENGTH; i < input.length; i++) {
  const slice = input.slice(i - MARKER_LENGTH, i)
  if (!hasDuplicate(slice)) {
    console.log(i)
    break
  }
}


function hasDuplicate (data: string) {
  return new Set(data).size !== data.length
}