import { readFile } from 'node:fs/promises'

const input = await readFile('data.txt', 'utf-8')

const monkeyJobs = new Map<string, string | number>()

for (const line of input.split('\n')) {
  const [name, operation] = line.split(': ')

  const right = parseInt(operation)

  if (isNaN(right)) {
    monkeyJobs.set(name, operation)
  } else {
    monkeyJobs.set(name, right)
  }
}

function getValue (name: string): number {
  const job = monkeyJobs.get(name)

  if (!job) {
    // I know...
    throw new Error('')
  }

  if (typeof job === 'number') {
    return job
  }

  const [left, operator, right] = job.split(' ')

  if (operator === '+') {
    return getValue(left) + getValue(right)
  }

  if (operator === '-') {
    return getValue(left) - getValue(right)
  }

  if (operator === '*') {
    return getValue(left) * getValue(right)
  }

  if (operator === '/') {
    return getValue(left) / getValue(right)
  }

  throw new Error('Not implemented')
}

interface Operation {
  leftValue: Operation | number | 'humn'
  rightValue: Operation | number | 'humn'
  operator: '+' | '-' | '/' | '*'
  name: string
}

function getOperation(name: string): Operation | number | 'humn' {
  const job = monkeyJobs.get(name)!
  
  if (name === 'humn') {
    return name
  }

  if (typeof job === 'number') {
    return job
  }

  const [left, operator, right] = job.split(' ')

  return {
    operator: operator as any,
    leftValue: getOperation(left),
    rightValue: getOperation(right),
    name
  }
}

monkeyJobs.delete('humn')

const operation = getOperation('pdzb')
const value = getValue('bhlw')

function computeMyValue (operation: Operation | number | 'humn', value: number): number {
  if (operation === 'humn') {
    return value
  }

  if (typeof operation === 'number') {
    return value
  }

  let leftValue = operation.leftValue
  let rightValue = operation.rightValue

  if (typeof leftValue !== 'number' && typeof rightValue !== 'number') {
    try {
      leftValue = getValue((leftValue as Operation).name)
    } catch(e) {}
    try {
      rightValue = getValue((rightValue as Operation).name)
    } catch (e) {}

  }

  const operator = operation.operator

  if (typeof leftValue === 'number') {
    if (operator === '+') {
      return computeMyValue(rightValue, value - leftValue)
    }
    if (operator === '-') {
      return computeMyValue(rightValue, leftValue - value)
    }
    if (operator === '*') {
      return computeMyValue(rightValue, value / leftValue)
    }
    if (operator === '/') {
      return computeMyValue(rightValue, leftValue / value)
    }
  }

  if (typeof rightValue === 'number') {
    if (operator === '+') {
      return computeMyValue(leftValue, value - rightValue)
    }
    if (operator === '-') {
      return computeMyValue(leftValue, value + rightValue)
    }
    if (operator === '*') {
      return computeMyValue(leftValue, value / rightValue)
    }
    if (operator === '/') {
      return computeMyValue(leftValue, value * rightValue)
    }
  }

  throw new Error('No implemented')
}

console.log(computeMyValue(operation, value))
