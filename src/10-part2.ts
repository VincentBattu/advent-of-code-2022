import { getLines } from './file-reader.js'

const lines = await getLines('10')

class Crt {
  public startOfSprite = 1

  public lineIndex = 0
  public columnIndex = 0

  private lines: (string[])[] = []

  public constructor () {
    for (let i = 0; i < 6; i++) {
      this.lines.push(new Array<string>(40).fill('.'))
    }
  }

  public drawPixel () {
    const isASpritePixel = this.columnIndex + 1 >= this.startOfSprite && this.columnIndex + 1 <= this.startOfSprite + 2
    this.lines[this.lineIndex][this.columnIndex] = isASpritePixel ? '#' : '.'

    this.columnIndex++
    if (this.columnIndex % 40 === 0) {
      this.columnIndex = 0
      this.lineIndex++
    }
  }

  public print () {
    let output = ''
    for (const line of this.lines) {
      for (const character of line) {
        output += character
      }
      output += '\n'
    }
    console.log(output)
  }
}

class Cpu {
  private xRegister = 1

  public constructor (private crt: Crt) {
  }

  public execute (line: string) {
    const parts = line.split(' ')

    const instruction: 'noop' | 'addx' = parts[0] as any
    
    if (instruction === 'noop') {
      this.nextClock()
      return
    }

    if (instruction === 'addx') {
      const argument = parseInt(parts[1], 10)

      this.nextClock()
      this.nextClock()
      this.xRegister += argument
      this.crt.startOfSprite = this.xRegister
    }
  }

  public nextClock () {
    this.crt.drawPixel()
  }
}


const crt = new Crt()
const cpu = new Cpu(crt)
for (let i = 0; i <lines.length; i++) {
  const line = lines[i]
  cpu.execute(line)
}
crt.print()
