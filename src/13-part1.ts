import fs from 'node:fs/promises'

let fileContent = await fs.readFile('./input/13.txt', 'utf-8')
fileContent = fileContent.replaceAll('\r\n', '\n')
const packetsDescriptions = fileContent.split('\n\n')

type Packet = (number | Packet)[]

function compare (packet1: Packet, packet2: Packet): number {
  const leftValue = packet1[0]
  const rightValue = packet2[0]

  if (leftValue === undefined && rightValue !== undefined) {
    return -1
  }

  if (rightValue === undefined && leftValue !== undefined) {
    return 1
  }

  if (rightValue === undefined && leftValue === undefined) {
    return 0
  }

  if (typeof leftValue === 'number' && typeof rightValue === 'number') {
    if (leftValue === rightValue) {
      return compare(packet1.slice(1), packet2.slice(1))
    }
    return leftValue - rightValue
  }

  if (Array.isArray(leftValue) && Array.isArray(rightValue)) {
    const result = compare(leftValue, rightValue)

    if (result === 0) {
      return compare(packet1.slice(1), packet2.slice(1))
    }
    return result
  }

  if (typeof leftValue === 'number' && Array.isArray(rightValue)) {
    const result = compare([leftValue], rightValue)
    if (result === 0) {
      return compare(packet1.slice(1), packet2.slice(1))
    }
    return result
  }

  if (Array.isArray(leftValue) && typeof rightValue === 'number') {
    const result = compare(leftValue, [rightValue])
    if (result === 0) {
      return compare(packet1.slice(1), packet2.slice(1))
    }
    return result
  }

  throw new Error('Cannot happen')
}

let sum = 0
for (let i = 0; i < packetsDescriptions.length; i++) {
  const [firstPacket, secondPacket] = packetsDescriptions[i].split('\n').map(el => JSON.parse(el) as Packet)

  const result = compare(firstPacket, secondPacket)
  if (result < 0) {
    sum += i + 1
  }
}
console.log(sum)

