import { getLines } from './file-reader.js'

interface Position {
  x: number
  y: number
}

interface Wind {
  position: Position
  direction: '>' | '<' | '^' | 'v'
}

interface MyNode {
  position: Position,
  winds: Wind[]
  elapsedMinutes: number
}

class PriorityQueue<T> {
  private items: { element: T, priority: number }[] = []

  public enqueue(element: T, priority: number) {
    let contain = false
    for (let i = 0; i < this.items.length; i++) {
      if (this.items[i].priority > priority) {
        this.items.splice(i, 0, { element, priority })
        contain = true
        break
      }
    }

    if (!contain) {
      this.items.push({ priority, element })
    }
  }

  public dequeue() {
    return this.items.shift()!.element
  }

  public isEmtpy() {
    return this.items.length === 0
  }
}

class ValleyMap {
  private minX = 0
  private maxX: number

  private minY = 0
  private maxY: number

  public startPosition!: Position

  public endPosition!: Position

  public initialWinds: Wind[] = []

  constructor(lines: string[]) {
    this.maxY = lines.length - 1
    this.maxX = lines[0].length - 1

    for (let x = 0; x < lines[0].length; x++) {
      if (lines[0][x] === '.') {
        this.startPosition = {
          x,
          y: 0
        }
        break
      }
    }

    for (let x = 0; x < lines[lines.length - 1].length; x++) {
      if (lines[lines.length - 1][x] === '.') {
        this.endPosition = {
          x,
          y: lines.length - 1
        }
        break
      }
    }

    for (let y = 1; y < lines.length - 1; y++) {
      for (let x = 1; x < lines[0].length - 1; x++) {
        if (lines[y][x] !== '.') {
          this.initialWinds.push({
            position: { x, y },
            direction: lines[y][x] as any
          })
        }
      }
    }
  }

  public toString(node: MyNode) {
    let result = ''

    for (let x = 0; x < this.maxX; x++) {
      if (node.position.x === x && node.position.y === 0) {
        result += 'E'
      } else {
        result += x === this.startPosition.x ? '.' : '#'
      }
    }

    result += '\n'

    for (let y = this.minY + 1; y <= this.maxY - 1; y++) {
      result += '#'
      for (let x = this.minX + 1; x <= this.maxX - 1; x++) {
        const wind = node.winds.find(wind => this.arePositionsEqual(wind.position, { x, y }))
        result += wind ? wind.direction : this.arePositionsEqual(node.position, { x, y }) ? 'E' : '.'
      }
      result += '#\n'
    }

    for (let x = 0; x < this.maxX; x++) {
      result += x === this.endPosition.x ? '.' : '#'
    }

    return result
  }

  // public calculateMinimumElapsedMinutes(): number {
  //   const frontier = new PriorityQueue<MyNode>()
  //   frontier.enqueue({ position: this.startPosition, winds: this.initialWinds, elapsedMinutes: 0 }, 0)

  //   const costSoFar = {
  //     [this.hash(this.startPosition)]: 0
  //   }

  //   while (!frontier.isEmtpy()) {
  //     const current = frontier.dequeue()
  //     // console.log(this.toString(current.winds, current.position))

  //     if (this.arePositionsEqual(current.position, this.endPosition)) {
  //       return current.elapsedMinutes
  //     }

  //     const currentPositionHash = this.hash(current.position)
  //     for (const next of this.getNeighbors(current)) {
  //       const newCost = costSoFar[currentPositionHash] + this.heuristic(next.position, current.position)
  //       const nextPositionHash = this.hash(next.position)
  //       if (!(nextPositionHash in costSoFar) || newCost < costSoFar[nextPositionHash]) {
  //         costSoFar[nextPositionHash] = newCost
  //         const priority = newCost
  //         frontier.enqueue(next, priority)
  //       }
  //     }
  //   }

  //   throw new Error('cannot happen')
  // }

  public calculateMinimumElapsedMinutesWithBfs (start: MyNode, end: Position): MyNode {
    const queue: MyNode[] =  [start]

    const visited = new Set<string>()
    while (queue.length !== 0) {
      const current = queue.shift()!

      if (this.arePositionsEqual(current.position, end)) {
        return current
      }

      for (const next of this.getNeighbors(current)) {
        const nextHash = this.hash(next)
        if (!visited.has(nextHash)) {
          queue.push(next)
          visited.add(nextHash)
        }
      }
    }
    throw new Error()
  }

  private getNeighbors(current: MyNode): MyNode[] {
    const neighbors: MyNode[] = []
    const nextWinds = this.getNextWinds(current.winds)

    neighbors.push({
      position: {
        x: current.position.x,
        y: current.position.y + 1
      },
      winds: nextWinds,
      elapsedMinutes: current.elapsedMinutes + 1
    })

    neighbors.push({
      position: {
        x: current.position.x,
        y: current.position.y - 1
      },
      winds: nextWinds,
      elapsedMinutes: current.elapsedMinutes + 1
    })

    neighbors.push({
      position: {
        x: current.position.x - 1,
        y: current.position.y
      },
      winds: nextWinds,
      elapsedMinutes: current.elapsedMinutes + 1
    })

    neighbors.push({
      position: {
        x: current.position.x + 1,
        y: current.position.y
      },
      winds: nextWinds,
      elapsedMinutes: current.elapsedMinutes + 1
    })

    neighbors.push({
      position: current.position,
      winds: nextWinds,
      elapsedMinutes: current.elapsedMinutes + 1
    })

    return neighbors.filter(neighbor => {
      return !nextWinds.some(wind => this.arePositionsEqual(wind.position, neighbor.position)) && (
        this.arePositionsEqual(neighbor.position, this.startPosition) ||
        this.arePositionsEqual(neighbor.position, this.endPosition) ||
        (neighbor.position.x > this.minX && neighbor.position.x < this.maxX && neighbor.position.y > this.minY && neighbor.position.y < this.maxY)
      )
    })
  }

  private getNextWinds(winds: Wind[]): Wind[] {
    const nextWinds: Wind[] = []

    for (const wind of winds) {
      let dx = wind.direction === '<' ? -1 : wind.direction === '>' ? 1 : 0
      let dy = wind.direction === '^' ? -1 : wind.direction === 'v' ? 1 : 0

      const nextWind = {
        ...wind,
        position: {
          x: wind.position.x + dx,
          y: wind.position.y + dy
        }
      }

      if (nextWind.position.x === 0) {
        nextWind.position.x = this.maxX - 1
      }
      if (nextWind.position.x === this.maxX) {
        nextWind.position.x = this.minX + 1
      }

      if (nextWind.position.y === 0) {
        nextWind.position.y = this.maxY - 1
      }
      if (nextWind.position.y === this.maxY) {
        nextWind.position.y = this.minY + 1
      }

      nextWinds.push(nextWind)
    }
    return nextWinds
  }

  private hash(node: MyNode) {
    return `${node.position.x};${node.position.y};${node.elapsedMinutes}`
  }

  private arePositionsEqual(first: Position, second: Position): boolean {
    return first.x === second.x && first.y === second.y
  }

  private heuristic(target: Position, current: Position) {
    return Math.abs(target.x - current.x) + Math.abs(target.y - current.y)
  }
}

const lines = await getLines('24')
const valleyMap = new ValleyMap(lines)
console.time('BFS')

let start: MyNode = { position: valleyMap.startPosition, winds: valleyMap.initialWinds, elapsedMinutes: 0 }
start = valleyMap.calculateMinimumElapsedMinutesWithBfs(start, valleyMap.endPosition)
start = valleyMap.calculateMinimumElapsedMinutesWithBfs(start, valleyMap.startPosition)
console.log(valleyMap.calculateMinimumElapsedMinutesWithBfs(start, valleyMap.endPosition).elapsedMinutes)

console.timeEnd('BFS')
